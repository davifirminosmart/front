/* eslint-disable react/prop-types */
import React, { Suspense } from 'react'
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom'
import { Loader } from 'components/loader'
import { NotificationContainer } from 'react-notifications'
import 'react-notifications/lib/notifications.css'
import Navbar from '../components/navbar'

const Login = React.lazy(() => import('pages/login'))
const Principal = React.lazy(() => import('pages/principal'))
const Users = React.lazy(() => import('pages/users'))
const LoginAdmin = React.lazy(() => import('pages/admin/login'))
const PrincipalAdmin = React.lazy(() => import('pages/admin/principal'))
const Seguros = React.lazy(() => import('pages/seguro'))

function PrivateRoute ({ component: Component, ...rest }) {
  const jwt = sessionStorage.getItem('jwt')
  return (
    <Route
      {...rest} render={props => (
        jwt ? <Component {...props} />
          : <Redirect to={{ pathname: '/', from: props.location }} />
      )}
    />
  )
}

function PrivateRouteAdmin ({ component: Component, ...rest }) {
  const jwt = sessionStorage.getItem('jwtAdmin')
  return (
    <Route
      {...rest} render={props => (
        jwt ? <Component {...props} />
          : <Redirect to={{ pathname: '/admin', from: props.location }} />
      )}
    />
  )
}

export function Routes () {
  return (
    <>
      <NotificationContainer />
      <Suspense fallback={<Loader />}>
        <BrowserRouter>
          <Navbar />
          <Switch>
            <Route exact path='/' component={Login} />
            <PrivateRoute path='/principal' component={Principal} />
            <PrivateRoute path='/usuarios' component={Users} />
            <PrivateRoute path='/seguros' component={Seguros} />
          </Switch>
        </BrowserRouter>
      </Suspense>
    </>
  )
}

export function AdminRoutes () {
  return (
    <>
      <NotificationContainer />
      <Suspense fallback={<Loader />}>
        <BrowserRouter>
          <Switch>
            <Route exact path='/admin' component={LoginAdmin} />
            <PrivateRouteAdmin path='/admin/principal' component={PrincipalAdmin} />
          </Switch>
        </BrowserRouter>
      </Suspense>
    </>
  )
}
