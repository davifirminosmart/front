import React from 'react'

function ImageLogin () {
  return (
    <>
      <svg xmlns='http://www.w3.org/2000/svg' width='100%' height='70%' viewBox='0 0 941.114 720.13'>
        <g id='Grupo_16085' data-name='Grupo 16085' transform='translate(0.161)'>
          <rect id='Retângulo_6227' data-name='Retângulo 6227' width='2.15' height='179.413' transform='translate(130.649 98.904)' fill='#f2f2f2' />
          <rect id='Retângulo_6228' data-name='Retângulo 6228' width='2.15' height='179.413' transform='translate(202.677 98.904)' fill='#f2f2f2' />
          <path id='Caminho_10461' data-name='Caminho 10461' d='M848,138' transform='translate(-110.914 -113.392)' fill='#3f3d56' />
          <path id='Caminho_10462' data-name='Caminho 10462' d='M1058.9,474.468c25.837,148.375,10.088,294.3-108.695,344.055S687.5,794.908,628.748,654.647s57.23-214.7,108.695-344.055C841.952,47.916,978.659,13.693,1058.9,474.468Z' transform='translate(-128.583 -114.815)' fill='#f6f8fd' />
          <circle id='Elipse_26' data-name='Elipse 26' cx='7.026' cy='7.026' r='7.026' transform='translate(824.8 596.039)' fill='#ff6584' />
          <line id='Linha_27' data-name='Linha 27' y1='79.995' transform='translate(866.247 639.682)' fill='#3f3d56' />
          <rect id='Retângulo_6229' data-name='Retângulo 6229' width='2.15' height='79.995' transform='translate(865.172 639.682)' fill='#3f3d56' />
          <circle id='Elipse_27' data-name='Elipse 27' cx='11.312' cy='11.312' r='11.312' transform='translate(854.934 628.369)' fill='#3f3d56' />
          <path id='Caminho_10463' data-name='Caminho 10463' d='M970.571,758.169s-1.616-34.758-34.745-30.718' transform='translate(-104.324 -69.185)' fill='#3f3d56' />
          <rect id='Retângulo_6230' data-name='Retângulo 6230' width='325' height='2' transform='translate(39.839 98.13)' fill='#f2f2f2' />
          <rect id='Retângulo_6231' data-name='Retângulo 6231' width='325' height='2' transform='translate(39.839 198.13)' fill='#f2f2f2' />
          <rect id='Retângulo_6232' data-name='Retângulo 6232' width='325' height='2' transform='translate(39.839 277.13)' fill='#f2f2f2' />
          <rect id='Retângulo_6233' data-name='Retângulo 6233' width='289.185' height='2.15' transform='translate(61.815 374.65)' fill='#3f3d56' />
          <path id='Caminho_10464' data-name='Caminho 10464' d='M205.638,483.661a18.276,18.276,0,1,1,18.276-18.276,18.276,18.276,0,0,1-18.276,18.276Zm0-34.4a16.126,16.126,0,1,0,16.126,16.126A16.126,16.126,0,0,0,205.638,449.259Z' transform='translate(-160.487 -90.197)' fill='#3f3d56' />
          <rect id='Retângulo_6234' data-name='Retângulo 6234' width='2.15' height='96.753' transform='translate(130.68 278.435)' fill='#3f3d56' />
          <rect id='Retângulo_6235' data-name='Retângulo 6235' width='2.15' height='176.306' transform='translate(202.708 198.882)' fill='#3f3d56' />
          <rect id='Retângulo_6236' data-name='Retângulo 6236' width='2.15' height='276.284' transform='translate(275.811 98.904)' fill='#3f3d56' />
          <circle id='Elipse_28' data-name='Elipse 28' cx='15.683' cy='15.683' r='15.683' transform='translate(116.041 359.505)' fill='#6c63ff' />
          <circle id='Elipse_29' data-name='Elipse 29' cx='15.683' cy='15.683' r='15.683' transform='translate(188.574 359.505)' fill='#6c63ff' />
          <circle id='Elipse_30' data-name='Elipse 30' cx='15.683' cy='15.683' r='15.683' transform='translate(261.108 359.505)' fill='#6c63ff' />
          <ellipse id='Elipse_31' data-name='Elipse 31' cx='15.5' cy='15' rx='15.5' ry='15' transform='translate(261.839 84.13)' fill='#6c63ff' />
          <circle id='Elipse_32' data-name='Elipse 32' cx='15.051' cy='15.051' r='15.051' transform='translate(261.919 183.714)' fill='#6c63ff' />
          <circle id='Elipse_33' data-name='Elipse 33' cx='15' cy='15' r='15' transform='translate(188.839 184.13)' fill='#6c63ff' />
          <circle id='Elipse_34' data-name='Elipse 34' cx='15.051' cy='15.051' r='15.051' transform='translate(188.816 263.266)' fill='#6c63ff' />
          <circle id='Elipse_35' data-name='Elipse 35' cx='15' cy='15' r='15' transform='translate(116.839 263.13)' fill='#6c63ff' />
          <circle id='Elipse_36' data-name='Elipse 36' cx='14' cy='14' r='14' transform='translate(-0.161 85.13)' fill='#f2f2f2' />
          <circle id='Elipse_37' data-name='Elipse 37' cx='14' cy='14' r='14' transform='translate(-0.161 184.13)' fill='#f2f2f2' />
          <circle id='Elipse_38' data-name='Elipse 38' cx='14' cy='14' r='14' transform='translate(-0.161 264.13)' fill='#f2f2f2' />
          <rect id='Retângulo_6237' data-name='Retângulo 6237' width='75.253' height='7.037' transform='translate(13.975)' fill='#3f3d56' />
          <rect id='Retângulo_6238' data-name='Retângulo 6238' width='75.253' height='7.037' transform='translate(13.975 15.832)' fill='#3f3d56' />
          <rect id='Retângulo_6239' data-name='Retângulo 6239' width='75.253' height='7.037' transform='translate(13.975 31.665)' fill='#3f3d56' />
          <path id='Caminho_10465' data-name='Caminho 10465' d='M799.926,217.318s20.322,26.038,17.782,44.454,41.914-25.4,41.914-25.4-15.242-31.753-6.986-46.995S799.926,217.318,799.926,217.318Z' transform='translate(-114.521 -109.781)' fill='#ffb9b9' />
          <path id='Caminho_10466' data-name='Caminho 10466' d='M871.7,705.452s26.673,29.213,22.227,35.564S811.37,771.5,802.479,770.228s-13.336-7.621-10.161-9.526,23.5-12.7,23.5-12.7l17.147-19.687s12.7-6.986,12.7-10.8S871.7,705.452,871.7,705.452Z' transform='translate(-115.169 -70.812)' fill='#2f2e41' />
          <path id='Caminho_10467' data-name='Caminho 10467' d='M825,735.579s-10.161,16.512-12.066,17.147,9.526,23.5,9.526,23.5l26.673-8.256L863.742,755.9l-3.175-16.512Z' transform='translate(-113.561 -68.551)' fill='#ffb9b9' />
          <path id='Caminho_10468' data-name='Caminho 10468' d='M801.315,442.574l-12.7,56.521s-43.819,98.435-13.336,115.582l66.682,127.648,35.564-15.877-59.7-130.823,27.308-150.51Z' transform='translate(-117.18 -90.537)' fill='#d0cde1' />
          <path id='Caminho_10469' data-name='Caminho 10469' d='M801.315,442.574l-12.7,56.521s-43.819,98.435-13.336,115.582l66.682,127.648,35.564-15.877-59.7-130.823,27.308-150.51Z' transform='translate(-117.18 -90.537)' opacity='0.1' />
          <path id='Caminho_10470' data-name='Caminho 10470' d='M803.337,451.11h-6.443a359.044,359.044,0,0,0,3.268,58.426c4.445,32.388,15.876,106.691,15.876,106.691s-1.27,13.971,2.54,18.417,4.445-1.27,5.081,8.891,6.668,80.807,6.668,80.807,4.128,14.452-.318,17.628-13.336,20.322-6.986,22.862,50.17,5.716,52.075,3.81,0-121.3,0-121.3-1.27-24.132-2.54-25.4-6.986-18.417-5.081-22.227.635-15.242.635-17.147-5.716,0-.635-8.256,5.716-74.3,5.716-74.3S900.5,458.1,892.246,447.3Z' transform='translate(-114.758 -90.183)' fill='#d0cde1' />
          <circle id='Elipse_39' data-name='Elipse 39' cx='41.914' cy='41.914' r='41.914' transform='translate(661.272 33.235)' fill='#ffb9b9' />
          <path id='Caminho_10471' data-name='Caminho 10471' d='M860.348,226.365s-25.4,6.351-35.564,21.592l-8.891,1.905v8.891s-16.512,52.71-12.7,75.573,0,67.317-6.351,79.383-8.256,14.607-5.081,19.687,7.621,22.227,4.445,29.848,0,15.242,29.213,17.147,70.492,1.905,70.492-8.256-2.54-38.739,0-46.36,3.81-12.7,2.54-19.687,2.54-85.1,2.54-85.1S901.627,242.876,860.348,226.365Z' transform='translate(-115.247 -106.761)' fill='#2f2e41' />
          <path id='Caminho_10472' data-name='Caminho 10472' d='M804.207,408.9l-34.929,52.075s-36.834,33.658-23.5,48.9,48.265-44.454,48.265-44.454l38.1-43.184Z' transform='translate(-118.803 -93.064)' fill='#ffb9b9' />
          <path id='Caminho_10473' data-name='Caminho 10473' d='M825.344,755.059s-1.27-8.256-5.716-8.256-34.294,20.322-34.294,20.322-40.975,17.628-23.675,18.709c17.2,1.075,108.138,3.518,108.773.343s1.905-34.488-2.54-34.391-40.009,7.083-40.009,7.083Z' transform='translate(-117.713 -67.709)' fill='#2f2e41' />
          <path id='Caminho_10474' data-name='Caminho 10474' d='M822.4,168.07a10.513,10.513,0,0,1-4.791,1.082c-1.713.1-3.758-.009-4.669-1.463a6.687,6.687,0,0,1-.583-3.05,10.048,10.048,0,0,0-11.742-8.558,14.453,14.453,0,0,0,1.871,5.959,12.745,12.745,0,0,1-11.2-1.491l-.645,8.554c-2.278,1.089-5.2,1.316-7.184-.242s-2.146-5.215.1-6.367c-2.776-.579-6.171-1.822-6.375-4.651-.186-2.581,2.553-4.469,3.058-7.007.411-2.07-.723-4.319.08-6.271,1.055-2.564,4.885-3.222,5.653-5.885.391-1.356-.172-2.807-.037-4.213.352-3.68,4.978-5.207,8.668-5s7.719,1.189,10.854-.769c1.52-.949,2.582-2.469,3.918-3.664a11.635,11.635,0,0,1,13.511-1.175c1.484.923,2.779,2.2,4.435,2.763,3.387,1.148,6.922-1.024,10.463-1.525a8.693,8.693,0,0,1,7.288,2.111,5.966,5.966,0,0,1,1.086,7.188c2.078-.351,4.279-.69,6.238.088s3.454,3.123,2.549,5.025a6.509,6.509,0,0,0-.79,1.614c-.184,1.107.882,2.053,1.958,2.371s2.244.258,3.3.644c4.353,1.593,3.309,8.552,6.735,11.674a27.319,27.319,0,0,0,3.714,2.271,9.037,9.037,0,0,1,3.187,10.918c-.687,1.521-1.837,2.972-1.7,4.635.194,2.332,2.693,3.61,4.447,5.16a10.347,10.347,0,0,1,2.409,11.88c-1.612,3.363-5.1,5.912-5.458,9.625-.154,1.614.331,3.225.3,4.846a9.984,9.984,0,0,1-4.292,7.538,22.322,22.322,0,0,1-8.238,3.428c-1.384.331-3.036.85-3.371,2.234-.4,1.64,1.433,3.133,1.244,4.81a2.789,2.789,0,0,1-1.12,1.813c-2.477,1.937-6.4.277-7.913-2.48s-1.321-6.1-1.086-9.232l.854-11.4c.266-3.548.24-7.762-2.652-9.835-3.2-2.291-7.855-.436-11.472-1.984-3.768-1.613-5.12-6.334-4.968-10.43s1.326-8.2.651-12.243c-.837-5.011-5.294-8.379-9.794-5.132C824.84,161.693,825.523,166.232,822.4,168.07Z' transform='translate(-116.229 -114.575)' fill='#2f2e41' />
          <path id='Caminho_10475' data-name='Caminho 10475' d='M856.018,254.744s-25.4,10.161-25.4,31.753,1.27,50.17-3.175,59.7-8.256,12.7-5.716,20.322,6.986,6.351,0,12.066-6.351-3.81-6.986,5.716,6.351,10.8,0,15.242-22.862,22.227-15.877,27.943,25.4,24.132,31.753,17.147,9.526-24.767,18.417-23.5,10.161,1.905,8.891-4.445-6.986-6.351.635-8.256,10.8,2.54,8.256-3.81-4.445-8.891.635-15.877,7.621-8.891,6.986-15.877,13.971-69.857,13.971-69.857S892.852,249.663,856.018,254.744Z' transform='translate(-114.728 -104.658)' opacity='0.1' />
          <path id='Caminho_10476' data-name='Caminho 10476' d='M849.52,253.562s-25.4,10.161-25.4,31.753,1.27,50.17-3.175,59.7-8.256,12.7-5.716,20.322,6.986,6.351,0,12.066-6.351-3.81-6.986,5.716,6.351,10.8,0,15.242-22.862,22.227-15.877,27.943,25.4,24.132,31.753,17.147,9.526-24.767,18.417-23.5,10.161,1.905,8.891-4.445-6.986-6.351.635-8.256,10.8,2.54,8.256-3.81-4.445-8.891.635-15.877,7.621-8.891,6.986-15.877,13.971-69.857,13.971-69.857S886.354,248.482,849.52,253.562Z' transform='translate(-115.216 -104.746)' opacity='0.1' />
          <path id='Caminho_10477' data-name='Caminho 10477' d='M852.474,251.79s-25.4,10.161-25.4,31.753,1.27,50.17-3.175,59.7-8.256,12.7-5.716,20.322,6.986,6.351,0,12.066-6.351-3.81-6.986,5.716,6.351,10.8,0,15.242-22.862,22.227-15.877,27.943,25.4,24.132,31.753,17.147,9.526-24.767,18.417-23.5,10.161,1.905,8.891-4.445-6.986-6.351.635-8.256,10.8,2.54,8.256-3.81-4.445-8.891.635-15.877,7.621-8.891,6.986-15.877,13.971-69.857,13.971-69.857S889.307,246.71,852.474,251.79Z' transform='translate(-114.994 -104.879)' fill='#2f2e41' />
          <line id='Linha_28' data-name='Linha 28' x1='580' transform='translate(296.839 719.13)' fill='none' stroke='#3f3d56' strokeMiterlimit='10' strokeWidth='2' />
          <rect id='Retângulo_6240' data-name='Retângulo 6240' width='75.253' height='7.037' transform='translate(319.286 513.868)' fill='#3f3d56' />
          <rect id='Retângulo_6241' data-name='Retângulo 6241' width='75.253' height='7.037' transform='translate(319.286 529.701)' fill='#3f3d56' />
          <rect id='Retângulo_6242' data-name='Retângulo 6242' width='75.253' height='7.037' transform='translate(319.286 545.533)' fill='#3f3d56' />
          <path id='Caminho_10478' data-name='Caminho 10478' d='M575.193,799.055a104.137,104.137,0,0,1-89.669-51.583l-.543-.932L574.385,695V591.645l1.078,0a103.7,103.7,0,0,1-.27,207.408Zm-87.262-51.734a101.559,101.559,0,1,0,88.6-153.515V696.239l-.538.31Z' transform='translate(-138.154 -79.351)' fill='#2f2e41' />
          <circle id='Elipse_40' data-name='Elipse 40' cx='39.021' cy='39.021' r='39.021' transform='translate(398.017 576.977)' fill='#6c63ff' />
          <circle id='Elipse_41' data-name='Elipse 41' cx='19.463' cy='19.463' r='19.463' transform='translate(417.576 596.536)' fill='#2f2e41' />
          <rect id='Retângulo_6243' data-name='Retângulo 6243' width='25.801' height='25.801' transform='translate(351.147 85.885)' fill='#6c63ff' />
          <rect id='Retângulo_6244' data-name='Retângulo 6244' width='25.801' height='25.801' transform='translate(351.147 184.789)' fill='#6c63ff' />
          <rect id='Retângulo_6245' data-name='Retângulo 6245' width='25.801' height='25.801' transform='translate(351.147 265.416)' fill='#6c63ff' />
          <path id='Caminho_10479' data-name='Caminho 10479' d='M515.951,478.951H488V451h27.951Zm-25.8-2.15H513.8V453.15H490.15Z' transform='translate(-137.928 -89.905)' fill='#3f3d56' />
        </g>
      </svg>

    </>
  )
}
export default ImageLogin
