import React, { useState } from 'react'
import ImageLogin from './imageLogin'
import Input from 'components/input'
import Button from 'components/button'
import LogoConductor from 'components/logoConductor'
import { Loader } from 'components/loader'
import { NotificationManager } from 'react-notifications'
import Api from 'api'
import Proptypes from 'prop-types'
import { useDispatch } from 'react-redux'
import { fillUser } from 'store/actions/actionsUser'
import { fillTiposBloqueio } from 'store/actions/actionsTipoBloqueio'

function Login ({ history }) {
  const dispatch = useDispatch()
  const [loading, setLoading] = useState(false)
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')

  function handleEmail (event) {
    const { value } = event.target
    setEmail(email => value)
  }

  function handlePassword (event) {
    const { value } = event.target
    setPassword(email => value)
  }

  async function handleClick () {
    setLoading((loading) => true)
    try {
      const { user, permissions, token: { token }, role, tiposBloqueio } = await Api.post('login', { email, password })
      sessionStorage.removeItem('jwtAdmin')
      Api.setJwt(token)
      setTimeout(() => {
        setLoading((loading) => false)
        history.push('/principal')
        dispatch(fillUser({ ...user, permissions, role }))
        dispatch(fillTiposBloqueio(tiposBloqueio))
      }, 1000)
    } catch (error) {
      NotificationManager.error(error.message)
      setTimeout(() => {
        setLoading((loading) => false)
      }, 1000)
    }
  }

  return (
    <>
      <div className='login'>
        <div className='login--image'>
          <ImageLogin />
        </div>
        <div className='login--form'>
          <div className='login--form--title'>
            <p className='logo'>
              <LogoConductor />
            </p>
            <p> Olá <span role='img' aria-label='emoji de asceno'> 👋</span>, </p>
            <p>antes de prosseguir preencha os dados por favor </p>
          </div>
          <div className='login--form--data'>
            <Input id='e-mail' label='Digite o seu e-mail' type='text' value={email} onChange={handleEmail} />
            <Input id='password' label='Digite a sua senha' type='password' value={password} onChange={handlePassword} />
          </div>
          <div className='login--form--footer'>
            {!loading && <Button onClick={handleClick}> Enviar </Button>}
            {loading && <Loader />}
          </div>
        </div>
      </div>
    </>
  )
}

Login.propTypes = {
  history: Proptypes.object.isRequired
}

export default Login
