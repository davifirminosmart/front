/* eslint-disable react/jsx-closing-tag-location */
import React, { useState } from 'react'
import NavBar from 'components/navbar'
import SearchBar from 'components/searchBar'
import Modal from 'react-responsive-modal'
import { useSelector, useDispatch } from 'react-redux'
import { changeModal, fillClient } from 'store/actions/actionsClient'
import { fillCard } from 'store/actions/actionsCard'
import { fillTicket } from 'store/actions/actionsTicket'
import { fillDependentes } from 'store/actions/actionsDependente'
import { fillCardDependente } from 'store/actions/actionsCardDependente'
import Button from 'components/button'
import { VerticalLoader } from 'components/loader'
import { NotificationManager } from 'react-notifications'
import MaskedInput from 'react-maskedinput'
import Input from 'components/input'
import Api from 'api'
import TimeLine from 'components/timeline'
import ProfileCard from 'components/profileCard'
import CreditCard from 'components/creditCard'
import Invoice from 'components/invoice'
import LastPurchase from 'components/lastPurchase'
import EditProfileCard from 'components/editProfileCard'
import DependentesTable from 'components/dependentesTable'
import 'rc-checkbox/assets/index.css'
import Checkbox from 'rc-checkbox'

function UserNotFound () {
  return (
    <>
      <div className='notfound'>
        <img alt='balões de chat' src='https://i.ibb.co/g9srHFh/pngwing-com.png' />
        <h1>Clique no menu de atender para começar um novo atendimento !!</h1>
      </div>
    </>
  )
}

function ClientModal () {
  const [loading, setLoading] = useState(false)
  const [findClient, setFindClient] = useState(false)
  const [cpf, setCPF] = useState('')
  const [client, setClient] = useState([])
  const [cards, setCards] = useState([])
  const [dependentes, setDependentes] = useState([])
  const [ansewrs, setAnsewers] = useState({ mes: '', ano: '' })
  const open = useSelector(state => state.clients.modal.open)
  const dispatch = useDispatch()
  const [mask, setMask] = useState('111.111.111-11')
  const [hasCpf, setHascpf] = useState(1)

  function handleCPF (event) {
    const { value } = event.target
    setCPF(_c => value)
  }

  async function searchClient () {
    try {
      setLoading(_l => true)
      setFindClient(_l => false)
      const data = await Api.get('client', { cpfCnpj: cpf.split('_').join('').split('.').join('').replace('-', '').replace('/', ''), hasCpf })
      setClient(_c => data.client)
      setCards(_c => data.card)
      console.log('Vou mandar os dependentes',data.dependentes)
      setDependentes(_d => data.dependentes)
      setLoading(_l => false)
      setFindClient(_l => true)
    } catch (error) {
      setLoading(_l => false)
      NotificationManager.error('Erro com a comunicação da api')
    }
  }

  function handleAnsewer (event, index) {
    const { value } = event.target
    setAnsewers(_a => ({
      ..._a,
      [index]: value
    })
    )
  }

  async function verifyAnswers () {
    console.log('respostas', ansewrs)
    console.log('cliente', client)
    if ((String(ansewrs.mes).toLowerCase() !== String(client[0].mesNascimento).toLowerCase()) || (Number(ansewrs.ano) !== Number(client[0].anoNascimento))) { throw new Error('Respostas erradas') }
  }

  async function createTicket () {
    setLoading(_l => true)
    try {
      await verifyAnswers()
      const { ticket } = await Api.post('ticket', { client: client[0] })
      setTimeout(() => {
        dispatch(fillClient(client[0]))
        dispatch(fillCard(cards[0]))
        dispatch(changeModal(false))
        dispatch(fillTicket(ticket))
        dispatch(fillCardDependente([]))
        dispatch(fillDependentes(dependentes))
        setLoading(_l => false)
        NotificationManager.success('Atendimento Iniciado')
      }, 2000)
    } catch (error) {
      NotificationManager.error(error.message)
      setLoading(_l => false)
    }
  }

  function changeDocument (status) {
    setHascpf(_h => status)
    if (status) {
      setMask(_m => '111.111.111-11')
    } else {
      setMask(_m => '11.111.111/1111-11')
    }
  }

  return (
    <Modal open={open} onClose={() => { dispatch(changeModal(false)) }} center classNames={{ modal: 'searchClientModal' }}>
      <div className='searchClientModal--title'>
        <span>Pesquise pelo documento de um cliente</span>
      </div>
      <div className='searchClientModal--document'>
        <div className='container--document'>
          <div className='document' onClick={() => { changeDocument(1) }}>
            CPF
            <Checkbox checked={hasCpf} />
          </div>
          <div className='document' onClick={() => { changeDocument(0) }}>
            CNPJ
            <Checkbox checked={!hasCpf} />
          </div>
        </div>
      </div>
      <span className='searchClientModal--input'>
        <MaskedInput type='text' mask={mask} value={cpf} onChange={handleCPF} />
        {!loading && <Button onClick={searchClient}>Pesquisar</Button>}
        {loading && <VerticalLoader />}
      </span>
      {findClient &&
        <>
          {client.length ? client.map((_c, index) =>
            <div key={index} className='searchClientModal--client'>
              <h2>Cliente Encontrado</h2>
              <div className='searchClientModal--client--data'>
                <div className='client--thumb'>
                  <i className='lni lni-user' />
                </div>
                <div className='client--fields'>
                  <div className='client--fields--row'>
                    <Input type='text' label='Nome' id='client-client' value={_c.name} disabled />
                  </div>
                  <div className='client--fields--row'>
                    <Input type='text' label='CPF' id='client-cpf' mask='111.111.111-11' value={_c.cpf} disabled />
                    <Input type='text' label='Email' id='client-email' value={_c.email} disabled />
                  </div>
                </div>
              </div>
              <div className='client--quests'>
                <h1>Perguntas</h1>
                <div className='client--quests-row'>
                  <h3>Qual o mês de nascimento do Cliente:</h3>
                  <Input type='text' label='Resposta' id='client-resp1' value={ansewrs.mes} onChange={event => handleAnsewer(event, 'mes')} />
                </div>
                <div className='client--quests-row'>
                  <h3>Qual o ano de nascimento do Cliente:</h3>
                  <Input type='text' label='Resposta' id='client-resp1' value={ansewrs.ano} onChange={event => handleAnsewer(event, 'ano')} />
                </div>
                <div className='client--fields--row centered'>
                  {!loading && <Button onClick={createTicket}>Atender</Button>}
                  {loading && <VerticalLoader />}
                </div>
              </div>
            </div>)
            : <div className='searchClientModal--client'>
              <h2>Cliente Não Encontrado</h2>
              <div className='searchClientModal--client--data'>
                <div className='client--thumb'>
                  <i className='lni lni-user' />
                </div>
                <div className='client--fields'>
                  <div className='client--fields--row'>
                    <Input type='text' label='Nome' id='client-client' />
                  </div>
                  <div className='client--fields--row'>
                    <Input type='text' label='CPF' id='client-cpf' mask='111.111.111-11' value={cpf} disabled />
                    <Input type='text' label='Email' id='client-email' />
                  </div>
                  <div className='client--fields--row centered'>
                    <Button>Criar</Button>
                  </div>
                </div>
              </div>
            </div>}
        </>}
    </Modal>
  )
}

function Principal ({ history }) {
  const protocol = useSelector(state => state.tickets.ticket.protocol)
  const clientData = useSelector(state => state.clients.client)
  console.log('aqui', clientData)
  return (
    <>
      <div className='principal'>
        <ClientModal />
        <div className='principal--search'>
          <SearchBar />
        </div>
        <div className='principal--body'>
          {protocol
            ? <div className='principal--body--container'>
              <div className='principal--body--cards'>
                <div className='row'>
                  <ProfileCard />
                  <EditProfileCard />
                  <div className='group'>
                    <CreditCard />
                  </div>
                </div>
                <div className='row'>
                  <DependentesTable />
                  <Invoice clientData={clientData} protocol={protocol} />
                  <LastPurchase clientData={clientData} protocol={protocol} />
                </div>
              </div>
              <div className='principal--body--timeline'>
                <TimeLine />
              </div>
            </div>
            : <UserNotFound />}
        </div>
      </div>
    </>
  )
}

export default Principal
