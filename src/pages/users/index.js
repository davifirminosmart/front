import React, { useState, useEffect } from 'react'
import NavBar from 'components/navbar'
import SearchBar from 'components/searchBar'
import Modal from 'react-responsive-modal'
import Api from 'api'
import Input from 'components/input'
import Button from 'components/button'
import { Loader } from 'components/loader'
import { NotificationManager } from 'react-notifications'
import { fillCardDependente } from 'store/actions/actionsCardDependente'
import { useDispatch } from 'react-redux'

export default function Users ({ history }) {
  const [modalNovoUsuario, setModalNovoUsuario] = useState(false)
  const [modalEditar, setModalEditarUsuario] = useState(false)
  const [modalGrupo, setModalGrupo] = useState(false)
  const [activeUser, setActiveUser] = useState({})
  const [usuarios, setUsuarios] = useState([])
  const [loading, setLoading] = useState(false)
  const [permissoes, setPermissoes] = useState([])
  const [grupos, setGrupos] = useState([])
  const [rolesSelect, setRolesSelect] = useState([])
  const dispatch = useDispatch()

  useEffect(() => {
    async function fetchData () {
      dispatch(fillCardDependente([]))
      let { users } = await Api.get('users')
      let { roles } = await Api.get('roles')
      setRolesSelect(_r => roles)
      const { permissions, groups } = await Api.get('permissions')
      setPermissoes(_p => permissions)
      setGrupos(_g => groups)
      users = users.map(user => {
        const data = new Date(user.birthday)
        user.birthday = data.toLocaleDateString()
        return user
      })
      setUsuarios(_u => users)
    } fetchData()
  }, [])

  function ModalEditUsuario () {
    const [user, setUser] = useState({
      username: '',
      email: '',
      cpf: '',
      birthday: '',
      password: '1234',
      group: '0'
    })

    useEffect(() => {
      async function fetchData () {
        console.log('Usuario ativo',activeUser)
        setUser(_u => activeUser)
      } fetchData()
    }, [activeUser])

    function handleUser (event) {
      event.persist()
      const { value, id } = event.target
      const key = id.split('_')[1]
      console.log(key)
      setUser(_u => (
        { ..._u, [key]: value }
      ))
    }

    function save () {
      if (user.id) {
        atualizar()
      } else {
        novo()
      }
    }

    async function atualizar () {
      try {
        setLoading(_l => true)
        const { dataUser } = await Api.put('users', { ...user })
        const formatedData = {
          ...user,
          id: dataUser.id
        }
        if (formatedData.group === '3') formatedData.name = 'Admin'
        if (formatedData.group === '4') formatedData.name = 'Consultivo'
        if (formatedData.group === '5') formatedData.name = 'Basico'
        if (formatedData.group === '6') formatedData.name = 'Avançado'
        setUsuarios(_u =>
          _u.map(us => {
            if (us.id === formatedData.id) return formatedData
            return us
          })
        )
        setLoading(_l => false)
        NotificationManager.success('Criado com Sucesso')
        handleModalEditarUsuario(false)
      } catch (error) {
        console.log(error)
        NotificationManager.error(error.message)
        setModalEditarUsuario(false)
        setLoading(_l => false)
      }
    }

    async function novo () {
      try {
        setLoading(_l => true)
        const { dataUser } = await Api.post('users', { ...user })
        const formatedData = {
          ...user,
          id: dataUser.id
        }
        if (formatedData.group === '3') formatedData.name = 'Admin'
        if (formatedData.group === '4') formatedData.name = 'Consultivo'
        if (formatedData.group === '5') formatedData.name = 'Basico'
        if (formatedData.group === '6') formatedData.name = 'Avançado'
        setUsuarios(_u => [..._u, formatedData])
        setLoading(_l => false)
        NotificationManager.success('Criado com Sucesso')
        setModalEditarUsuario(false)
        handleModalEditarUsuario(false)
      } catch (error) {
        console.log(error)
        NotificationManager.error(error.message)
        setLoading(_l => false)
      }
    }

    return (
      <>
        <Modal open={modalEditar} onClose={() => { handleModalEditarUsuario(false) }} center classNames={{ modal: 'modal--editar' }}>
          <h1>Novo Usuário</h1>
          <div className='modal--editar--body'>
            <fieldset>
              <legend>Usuário</legend>
              <div className='row'>
                <Input type='text' label='Nome' id='user_username' value={user.username} onChange={handleUser} />
              </div>
              <div className='row'>
                <Input type='text' label='Email' id='user_email' value={user.email} onChange={handleUser} />
              </div>
              <div className='row'>
                <Input type='text' mask='111.111.111-11' id='user_cpf' label='CPF' value={user.cpf} onChange={handleUser} />
              </div>
              <div className='row'>
                <Input type='text' mask='11/11/1111' id='user_birthday' label='Nascimento' value={user.birthday} onChange={handleUser} />
                <Input type='password' id='user_password' label='senha' value={user.password} onChange={handleUser} />
              </div>
              <div className='row row--select'>
                <label>Grupo</label>
                <select onChange={handleUser} id='user_group' value={user.group}>
                  <option value='0' selected>Selecione</option>
                  {rolesSelect.map((role, index) => <option key={index} value={role.id}>{role.name}</option>)}
                </select>
              </div>
              <div className='row row--button'>
                {!loading && <Button onClick={save}>Salvar</Button>}
                {loading && <Loader />}
              </div>
            </fieldset>
          </div>
        </Modal>
      </>
    )
  }

  function ModalNovoUsuario () {
    return (
      <>
        <Modal open={modalNovoUsuario} onClose={() => { handleModalUsuario(false) }} center classNames={{ modal: 'modal--usuario' }}>
          <div className='modal--usuario--novo'>
            <span onClick={() => {
              handleModalEditarUsuario(true); setActiveUser(u => ({
                username: '',
                email: '',
                cpf: '',
                birthday: '',
                password: '',
                group: '0'
              }))
            }}
            >Novo Usuário
            </span>
          </div>
          <div className='modal--usuario--edit'>
            <table>
              <tr>
                <th>Nome</th>
                <th>E-mail</th>
                <th>CPF</th>
                <th>Perfil</th>
                <th>Editar</th>
              </tr>
              {usuarios.map((_u, index) =>
                <tr key={index}>
                  <td>{_u.username}</td>
                  <td>{_u.email}</td>
                  <td>{_u.cpf}</td>
                  <td>{_u.name}</td>
                  <td><span onClick={() => { handleModalEditarUsuario(true); setActiveUser(u => _u) }}>Editar</span></td>
                </tr>
              )}
            </table>
          </div>
        </Modal>
      </>
    )
  }

  function handleModalEditarUsuario (status) {
    setModalEditarUsuario(_m => status)
  }

  function handleModalUsuario (status) {
    setModalNovoUsuario(_s => status)
  }

  function handleModalGrupo (status) {
    setModalGrupo(_s => status)
  }

  function ModalGrupoUsuario () {
    const [selectedGroup, setSelectedGroup] = useState(0)
    const [activePermissions, setActivePermissions] = useState([])
    const [loading, setLoading] = useState(false)

    useEffect(() => {
      async function fetchData () {
        if (Number(selectedGroup) === 0) setActivePermissions(_p => [])
        const { permissions } = await Api.get(`permissions/${selectedGroup}`)
        if (permissions.length) setActivePermissions(_p => permissions)
      } fetchData()
    }, [selectedGroup])

    function handleSelect (event) {
      const { value } = event.target
      setSelectedGroup(_g => value)
    }

    function removePermission (id) {
      console.log(id)
      setActivePermissions(_p => _p.filter(p => p !== id))
    }

    function addPermissions (id) {
      setActivePermissions(_p => [..._p, id])
    }

    async function save () {
      try {
        if (Number(selectedGroup) === 0) throw new Error('Selecione um grupo')
        setLoading(_l => true)
        await Api.put('permissions', { permissions: activePermissions, id: selectedGroup })
        NotificationManager.success('Atualizado com sucesso')
        setLoading(_l => false)
      } catch (error) {
        NotificationManager.error(error.message)
        setLoading(_l => false)
      }
    }
    return (
      <>
        <Modal open={modalGrupo} onClose={() => { handleModalGrupo(false) }} center classNames={{ modal: 'modal--grupo' }}>
          <div className='modal--grupo--titulo'>
            <h1>Grupos</h1>
          </div>

          <div className='modal--grupo--body'>
            <div className='group--groups'>
              <label>Grupo</label>
              <select value={selectedGroup} onChange={handleSelect}>
                <option value='0'>Selecionar</option>
                {rolesSelect.map((role, index) => <option key={index} value={role.id}>{role.name}</option>)}
              </select>
            </div>

            <div className='group--container'>
              {grupos.map((_g, index) => {
                const permissions = permissoes.filter(_p => _p.description === _g)
                return (
                  <React.Fragment key={index}>
                    <div className='group'>
                      <h2>{_g}</h2>
                    </div>
                    <div className='group--permissions'>
                      {permissions.map((_p, index) => activePermissions.includes(_p.id)
                        ? <span onClick={() => { removePermission(_p.id) }} key={index} className='active'>{_p.name}</span>
                        : <span onClick={() => { addPermissions(_p.id) }} key={index}>{_p.name}</span>
                      )}
                    </div>
                  </React.Fragment>
                )
              })}
            </div>
            <div className='group--save' onClick={save}>
              {!loading && <span>Salvar</span>}
              {loading && <Loader />}
            </div>
          </div>

        </Modal>
      </>
    )
  }
  return (
    <>
      <ModalNovoUsuario />
      <ModalEditUsuario />
      <ModalGrupoUsuario />
      <div className='users'>
        <div className='users--search'>
          <SearchBar />
        </div>
        <div className='users--body'>
          <div className='users--body--new'>
            <span onClick={() => { handleModalUsuario(true) }}>Novo/Editar usuarios</span>
          </div>
          <div className='users--body--new'>
            <span onClick={() => { handleModalGrupo(true) }}>Editar Grupo de usuários</span>
          </div>
        </div>
      </div>
    </>
  )
}
