import React from 'react'
import NavBar from 'components/navbar'
import ProfileCard from 'components/profileCard'
import EditProfileCard from 'components/editProfileCard'
import SegurosContratados from 'components/SegurosContratados'
import SegurosDisponiveis from 'components/SegurosDisponiveis'
import SegurosCancelados from 'components/SegurosCancelados'
import { useSelector, useDispatch } from 'react-redux'

function UserNotFound () {
  return (
    <>
      <div className='notfound'>
        <img alt='balões de chat' src='https://i.ibb.co/g9srHFh/pngwing-com.png' />
        <h1>Clique no menu de atender para começar um novo atendimento !!</h1>
      </div>
    </>
  )
}

function Seguro ({ history }) {
  const protocol = useSelector(state => state.tickets.ticket.protocol)
  return (
    <div className='seguros'>
      <div className='seguros--body'>
        <div className='seguros--body--header'/>
        { protocol
        ? <>
        <div className='row first-row'>
          <ProfileCard />
          <EditProfileCard />
          <SegurosContratados />
        </div>
        <div className='row'>
          <SegurosDisponiveis />
        </div>
        <div className='row'>
          <SegurosCancelados />
        </div>
        </>
        : <UserNotFound /> }
      </div>
    </div>
  )
}

export default Seguro
