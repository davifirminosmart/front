import React, { useState } from 'react'
import Button from 'components/button'
import { NotificationManager } from 'react-notifications'
import Api from 'api'
import { Loader } from 'components/loader'

function AdminPrincipal () {
  const [company, setCompany] = useState({ name: '', cnpj: '', 'help-desk': false, tokenHelpDesk: '' })
  const [user, setUser] = useState({ username: '', cpf: '', email: '', birthday: '', password: '' })
  const [loading, setLoading] = useState(false)

  async function changeCompany (event) {
    const { id, value } = event.target
    await setCompany(_c => ({ ..._c, [id]: value }))
  }

  async function changeCompanyHelpDesk (event) {
    const { id, checked } = event.target
    await setCompany(_c => ({ ..._c, [id]: checked }))
  }

  async function changeUser (event) {
    const { id, value } = event.target
    await setUser(_u => ({ ..._u, [id]: value }))
  }

  async function send () {
    setLoading(_l => true)
    try {
      await Api.post('admin/empresa', { user, company })
      NotificationManager.success('Criado com Sucesso')
      setLoading(_l => false)
    } catch (error) {
      NotificationManager.error(error.message)
      setLoading(_l => false)
    }
  }

  return (
    <>
      <div className='adminPrincipal'>
        <div className='adminPrincipal--title'>
          <h1>Cadastro de Empresa</h1>
          <div className='adminPrincipal--body'>
            <div className='adminPrincipal--body--input'>
              <label htmlFor='name'> Nome </label>
              <input type='text' id='name' value={company.name} onChange={changeCompany} />
            </div>
            <div className='adminPrincipal--body--input'>
              <label htmlFor='cnpj'> Cnpj </label>
              <input type='text' id='cnpj' value={company.cnpj} onChange={changeCompany} />
            </div>
            <div className='adminPrincipal--body--input'>
              <label htmlFor='help-desk'> Possui integração </label>
              <input type='checkbox' id='help-desk' checked={company['help-desk']} onChange={changeCompanyHelpDesk} />
            </div>
            {company['help-desk'] && <div className='adminPrincipal--body--input'>
              <label htmlFor='help-desk'> Token Help Desk </label>
              <input type='text' id='tokenHelpDesk' value={company.tokenHelpDesk} onChange={changeCompany} />
            </div> }
            <div className='adminPrincipal--body--input'>
              <label htmlFor='userConductor'> Usuário Conductor </label>
              <input type='text' id='userConductor' value={company.userConductor} onChange={changeCompany} />
            </div>
            <div className='adminPrincipal--body--input'>
              <label htmlFor='passwordConductor'> Senha Conductor </label>
              <input type='text' id='passwordConductor' value={company.passwordConductor} onChange={changeCompany} />
            </div>
          </div>
        </div>

        <div className='adminPrincipal--title'>
          <h1>Cadastro de Usuário</h1>
          <div className='adminPrincipal--body'>
            <div className='adminPrincipal--body--input'>
              <label htmlFor='name'> Nome </label>
              <input type='text' id='username' value={user.username} onChange={changeUser} />
            </div>
            <div className='adminPrincipal--body--input'>
              <label htmlFor='cpf'> CPF </label>
              <input type='text' id='cpf' value={user.cpf} onChange={changeUser} />
            </div>
            <div className='adminPrincipal--body--input'>
              <label htmlFor='email'> Email </label>
              <input type='text' id='email' value={user.email} onChange={changeUser} />
            </div>
            <div className='adminPrincipal--body--input'>
              <label htmlFor='birthday'> Data de nascimento </label>
              <input type='text' id='birthday' value={user.birthday} onChange={changeUser} />
            </div>
            <div className='adminPrincipal--body--input'>
              <label htmlFor='password'> Senha </label>
              <input type='text' id='password' value={user.password} onChange={changeUser} />
            </div>
          </div>
          <div className='adminPrincipal--footer'>
            {!loading && <Button onClick={send}> Enviar </Button>}
            {loading && <Loader />}
          </div>
        </div>

      </div>
    </>
  )
}

export default AdminPrincipal
