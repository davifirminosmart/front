import React, { useEffect, useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { addCardDependente, alterCardDependente } from 'store/actions/actionsCardDependente'
import { alterCard } from 'store/actions/actionsCard'
import { NotificationManager } from 'react-notifications'
import Api from 'api'
import Can from 'layout/can'
import Modal from 'react-responsive-modal'
import Moment from 'moment'
import Input from 'components/input'
import Button from 'components/button'
import { Loader } from 'components/loader'
import ModalDependente from './modalDependente'

function CreditCard () {
  const dispatch = useDispatch()
  const cards = useSelector(state => state.cards.cards)
  const cardsDependente = useSelector(state => state.cardsDepentende.cards)
  const dependentes = useSelector(state => state.dependentes.dependentes)
  const client = useSelector(state => state.clients.client)
  const ticket = useSelector(state => state.tickets.ticket)
  const [numberCard, setNumberCard] = useState(0)
  const [modalInformacoes, setModalInformacoes] = useState(false)
  const [modalBloquear, setModalBloquear] = useState(false)
  const [modalDesbloquear, setModalDesbloquear] = useState(false)
  const [modalSegVia, setModalSegVia] = useState(false)
  const [modalAnuidade, setModalAnuidade] = useState(false)
  const [modalAdicionar, setModalAdicionar] = useState(false)
  const [modalLimite, setModalLimite] = useState(false)
  const [modalEspecial, setModalEspecial] = useState(false)
  const [modalContestacao, setModalContestacao] = useState(false)
  const [modalPassword, setModalPassword] = useState(false)
  const [modalHist, setModalHist] = useState(false)
  const [cardSelected, setCardSelected] = useState()
  const [timeline, setTimeLine] = useState([])

  useEffect(() => {
    async function fetchData () {
      if (dependentes.length) {
        console.log('dependeteees', dependentes)
        dependentes.map(async _d => {
          const { cards } = await Api.get('adicionais', { idClienteDependente: _d.id })
          if (cards) dispatch(addCardDependente(cards))
        })
      }
    } fetchData()
  }, [dependentes, dispatch])

  function changeModalEspecial (status) {
    setModalEspecial(_s => status)
  }

  function changeModalContestacao (status) {
    setModalContestacao(_s => status)
  }

  function changeModalPassword (status) {
    setModalPassword(_s => status)
  }

  function changeModalAnuidade (status) {
    setModalAnuidade(_a => status)
  }

  function changeModalHistorico (status) {
    setModalHist(_m => status)
  }

  function changeModalBloquear (status) {
    setModalBloquear(_m => status)
  }

  function changeModalDesbloquear (status) {
    setModalDesbloquear(_m => status)
  }

  function changeModalLimite (status) {
    setModalLimite(_m => status)
  }

  function changeModalInformacoes (status, number) {
    setModalInformacoes(_m => status)
    setNumberCard(_n => number)
  }

  function changeModalAdicionar (status) {
    setModalAdicionar(_m => status)
  }

  function ModalInformacoes () {
    return (
      <>
        {cardSelected &&
          <Modal open={modalInformacoes} onClose={() => { changeModalInformacoes(false, 0) }} center classNames={{ modal: 'modalInformacoes--card' }}>
            <div className={`creditcard creditcard--${numberCard} largeCard`}>
              <div className='largeCard--image'>
                <img src='https://www.elo.com.br/themes/custom/elo/img/logo_topo.png' alt='logo cartao' />
              </div>
              <div className='largeCard--data'>
                <div>
                  <span>Melhor dia de compra:</span>
                  <span>{cardSelected.melhorDia}</span>
                </div>
                <div>
                  <span>Saldo atual final:</span>
                  <span>R$ {Number(Number(cardSelected.limite) - Number(cardSelected.saldoDisponivel)).toFixed(2)}</span>
                </div>
                <div>
                  <span>Valor mínimo a pagar:</span>
                  <span>R$ {cardSelected.minimo}</span>
                </div>
                <div>
                  <span>Valor último pagamento:</span>
                  <span>R$ {cardSelected.ultimoPagamento}</span>
                </div>
                <div>
                  <span>Saldo último extrado:</span>
                  <span>R$ {cardSelected.saldoUltimoExtrato}</span>
                </div>
                <div>
                  <span>Ultimo extrato Enviado:</span>
                  <span>{cardSelected.ultimoExtratoEnviado}</span>
                </div>
                <div>
                  <span>Compras no período:</span>
                  <span>R$ {cardSelected.comprasPeriodo}</span>
                </div>
                <div>
                  <span>Saldo Disponível:</span>
                  <span>R$ {cardSelected.saldoDisponivel}</span>
                </div>
                <div>
                  <span>Limite à vista:</span>
                  <span>R$ {cardSelected.limiteAvista}</span>
                </div>
                <div>
                  <span>Limite Saque:</span>
                  <span>R$ {cardSelected.limiteSaque}</span>
                </div>
              </div>
            </div>
          </Modal>}
      </>
    )
  }

  function ModalBlock () {
    const tipos = useSelector(state => state.tipoBloqueio.tipos)
    const [tipoSelecionado, setTipoSelecionado] = useState(0)
    const [loading, setLoading] = useState(false)
    const [obs, setObs] = useState('')

    function handleTipo (event) {
      const { value } = event.target
      setTipoSelecionado(_t => value)
    }

    function handleObs (event) {
      const { value } = event.target
      setObs(_o => value)
    }

    async function bloquear () {
      setLoading(_l => true)
      try {
        if (!obs || !Number(tipoSelecionado)) throw new Error('Preencha todos os dados')
        await Api.post('bloquear', { id: cardSelected.id, cpf: client.cpf, protocolo: ticket.protocol, id_conductor: client.id, tipo: tipoSelecionado, observacao: obs })
        NotificationManager.success('Bloqueado com Sucesso')
        setLoading(_l => false)
        changeModalBloquear(false)
        if(cardSelected.dependente) {
          dispatch(alterCardDependente({
              ...cardSelected,
              bloqueado: 1,
              obsBloqueio: obs
            }))
        } else {
          dispatch(alterCard({
            ...cardSelected,
            bloqueado: 1,
            obsBloqueio: obs
          }))
        }

      } catch (error) {
        setLoading(_l => false)
        NotificationManager.error(error.message)
      }
    }

    return (
      <>
        <Modal open={modalBloquear} onClose={() => { changeModalBloquear(false) }} center classNames={{ modal: 'modalBloquear--card' }}>
          <div className='modalBloquear--card--title'>
            <h1>Deseja bloquear o cartão</h1>
          </div>
          <div className='modalBloquear--card--body'>
            <div className='row row-select'>
              <label>Tipo de bloqueio:</label>
              <select onChange={handleTipo} value={tipoSelecionado}>
                <option value='0'>Selecione um tipo</option>
                {tipos.map(_t => <option key={_t.id} value={_t.id}> {_t.descricao}</option>)}
              </select>
            </div>
            <div className='row'>
              <label>Observação:</label>
              <textarea value={obs} onChange={handleObs} />
            </div>
          </div>
          <div className='modalBloquear--card--buttons'>
            {!loading && <Button onClick={bloquear}>Bloquear</Button>}
          </div>
          {loading && <div style={{ display: 'flex', border: 'none', justifyContent: 'center' }}><Loader /> </div>}
        </Modal>
      </>
    )
  }

  function ModalPassword () {
    const [loading, setLoading] = useState(false)

    async function send () {
      setLoading(_l => true)
      try {
        await Api.post('senhasms', { id: cardSelected.id })
        NotificationManager.success('Enviado com Sucesso')
        changeModalPassword(false)
        setLoading(_c => false)
      } catch (error) {
        NotificationManager.error('Falha ao Bloquear')
        setLoading(_c => false)
      }
    }

    return (
      <Modal open={modalPassword} onClose={() => { changeModalPassword(false) }} center classNames={{ modal: 'modalPassword' }}>
        <div className='modalPassword--title'>
          <h1>Deseja Enviar a senha por SMS ?</h1>
        </div>
        <div className='modalPassword--footer'>
          {!loading && <Button onClick={send}>Enviar</Button> }
        </div>
        {loading && <div style={{ display: 'flex', border: 'none', justifyContent: 'center' }}><Loader /> </div>}
      </Modal>
    )
  }

  function ModalLimite () {
    const [loading, setLoading] = useState(false)
    const [obs, setObs] = useState('')

    function handleObs (event) {
      const { value } = event.target
      setObs(_o => value)
    }

    async function alterar () {
      setLoading(true)
      try {
        await Api.post('limite', { card: cardSelected, cpf: client.cpf, name: client.name, protocolo: ticket.protocol, id_conductor: client.id, email: client.email, obs })
        NotificationManager.success('Ticket Criado com Sucesso')
        setLoading(false)
        changeModalLimite(false)
      } catch (error) {
        setLoading(false)
        NotificationManager.error(error.message)
      }
    }

    return (
      <>
        <Modal open={modalLimite} onClose={() => { changeModalLimite(false) }} center classNames={{ modal: 'modalLimite--card' }}>
          <div className='modalLimite--card--header'>
            <h1>Alterar o limite do cartão?</h1>
          </div>
          <div className='row'>
            <label>Observação:</label>
            <textarea onChange={handleObs} value={obs} />
          </div>

          <div className='modalLimite--card--buttons'>
            {!loading && <Button onClick={alterar}>Alterar</Button>}
          </div>
          {loading && <div style={{ display: 'flex', border: 'none', justifyContent: 'center' }}><Loader /> </div>}
        </Modal>
      </>
    )
  }

  function ModalDesbloquear () {
    const [obs, setObs] = useState('')
    const [loading, setLoading] = useState(false)

    function handleObs (event) {
      const { value } = event.target
      setObs(_o => value)
    }

    async function desbloquear () {
      setLoading(_l => true)
      try {
        if (!obs) throw new Error('Preencha todos os dados')
        await Api.post('desbloquear', { id: cardSelected.id, cpf: client.cpf, protocolo: ticket.protocol, id_conductor: client.id, observacao: obs })
        NotificationManager.success('Desbloqueado com Sucesso')
        setLoading(_l => false)
        changeModalDesbloquear(false)
      } catch (error) {
        NotificationManager.error(error.message)
        setLoading(_l => false)
      }
    }

    return (
      <>
        <Modal open={modalDesbloquear} onClose={() => { changeModalDesbloquear(false) }} center classNames={{ modal: 'modalDesbloquear--card' }}>
          <div className='modalDesbloquear--card--title'>
            <h1>Atenção</h1>
            <span>O cartão está bloqueado pelo seguinte motivo:</span>
          </div>
          <div className='modalDesbloquear--card--body'>
            {cardSelected && <span>{cardSelected.obsBloqueio}</span>}
          </div>
          <div className='row'>
            <label>Observação:</label>
            <textarea onChange={handleObs} value={obs} />
          </div>
          <div className='modalDesbloquear--card--buttons'>
            {!loading && <Button onClick={desbloquear}>Desbloquear</Button>}
          </div>
          {loading && <div style={{ display: 'flex', border: 'none', justifyContent: 'center' }}><Loader /> </div>}
        </Modal>
      </>
    )
  }

  function ModalAnuidade () {
    const [loading, setLoading] = useState(false)

    async function cancelarAnuidade () {
      setLoading(_l => true)
      try {
        const id = cardSelected.idResponsavel
        await Api.post('cancelarAnuidade', { id })
        NotificationManager.success('Cancelado com Sucesso')
        setLoading(_l => false)
        changeModalAnuidade(false)
      } catch (error) {
        setLoading(_l => false)
        NotificationManager.error(error.message)
      }
    }
    return (
      <>
        <Modal open={modalAnuidade} onClose={() => { changeModalAnuidade(false) }} center classNames={{ modal: 'modalAnuidade--card' }}>
          <div className='modalAnuidade--card--title'>
            <h1>Deseja cancelar a anuidade do cartão</h1>
            <h2>Anuidade atual:</h2>
            {cardSelected && <h2>R$ {cardSelected.anuidade}</h2>}
          </div>
          <div className='modalAnuidade--card--buttons'>
            {!loading && <div onClick={cancelarAnuidade}>Sim</div>}
          </div>
          {loading && <div style={{ display: 'flex', border: 'none', justifyContent: 'center' }}><Loader /> </div>}
        </Modal>
      </>
    )
  }

  function ModalEspecial () {
    const [loading, setLoading] = useState(false)
    const [obs, setObs] = useState('')

    function handleObs (event) {
      const { value } = event.target
      setObs(_o => value)
    }

    async function alterar () {
      setLoading(true)
      try {
        await Api.post('solicitacao', { card: cardSelected, cpf: client.cpf, name: client.name, protocolo: ticket.protocol, id_conductor: client.id, email: client.email, obs })
        NotificationManager.success('Ticket Criado com Sucesso')
        setLoading(false)
        changeModalEspecial(false)
      } catch (error) {
        setLoading(false)
        NotificationManager.error(error.message)
      }
    }

    return (
      <>
        <Modal open={modalEspecial} onClose={() => { changeModalEspecial(false) }} center classNames={{ modal: 'modalLimite--card' }}>
          <div className='modalLimite--card--header'>
            <h1>Solicitação Especial</h1>
            <div className='row'>
              <label>Observação:</label>
              <textarea value={obs} onChange={handleObs} />
            </div>
          </div>
          <div className='modalLimite--card--buttons'>
            {!loading && <Button onClick={alterar}>Solicitar</Button>}
          </div>
          {loading && <div style={{ display: 'flex', border: 'none', justifyContent: 'center' }}><Loader /> </div>}
        </Modal>
      </>
    )
  }

  function ModalContestacao () {
    const [loading, setLoading] = useState(false)
    const [obs, setObs] = useState('')

    function handleObs (event) {
      const { value } = event.target
      setObs(_o => value)
    }

    async function alterar () {
      setLoading(true)
      try {
        await Api.post('contestacao', { card: cardSelected, cpf: client.cpf, name: client.name, protocolo: ticket.protocol, id_conductor: client.id, email: client.email, obs })
        NotificationManager.success('Ticket Criado com Sucesso')
        setLoading(false)
        changeModalContestacao(false)
      } catch (error) {
        setLoading(false)
        NotificationManager.error(error.message)
      }
    }

    return (
      <>
        <Modal open={modalContestacao} onClose={() => { changeModalContestacao(false) }} center classNames={{ modal: 'modalLimite--card' }}>
          <div className='modalLimite--card--header'>
            <h1 style={{ width: '100%', textAlign: 'center' }}>Contestação?</h1>
          </div>

          <div className='row'>
            <label>Observação:</label>
            <textarea value={obs} onChange={handleObs} />
          </div>

          <div className='modalLimite--card--buttons'>
            {!loading && <Button onClick={alterar}>Contestar</Button>}
          </div>
          {loading && <div style={{ display: 'flex', border: 'none', justifyContent: 'center' }}><Loader /> </div>}
        </Modal>
      </>
    )
  }

  async function fillHistorico (card) {
    try {
      console.log('cartao selecionado', card)
      const { timeline } = await Api.get(`cardHistory?id=${card.id}`)
      setTimeLine(_t => timeline)
    } catch (error) {
      NotificationManager.error(error.message)
    }
  }

  function ModalHistorico () {
    return (
      <>
        {cardSelected &&
          <Modal open={modalHist} onClose={() => { changeModalHistorico(false) }} center classNames={{ modal: 'modalHistorico--card' }}>
            <div className='modalHistorico--card--title' style={{ paddingTop: '10px' }}>
              <h1>Histórico do cartão com código {cardSelected.id} </h1>
            </div>
            <div className='modalHistorico--card--timeLine'>
              <ul className='timeline'>
                {timeline.map((item, index) =>
                  <li className='timeline-item' key={index}>
                    <div className='timeline-info'>
                      <span>{Moment(item.created_at).format('DD/MM/YYYY HH:mm')}</span>
                    </div>
                    <div className='timeline-marker' />
                    <div className='timeline-content'>
                      <h3 className='timeline-title'>{item.title}</h3>
                      <p>{item.description}</p>
                      <p className='created'> {item.username}</p>
                      <p className='protocol'>{item.protocol}</p>
                    </div>
                  </li>)}
                  {!timeline.length && <div className='timeline-empty'>
                    <h1>Não foram encontrado registros para esse cartão</h1>
                  </div>}
              </ul>
            </div>
          </Modal>}
      </>
    )
  }

  return (
    <>
      <ModalInformacoes />
      <ModalBlock />
      <ModalHistorico />
      <ModalDependente open={modalAdicionar} close={changeModalAdicionar} client={client}/>
      <ModalAnuidade />
      <ModalDesbloquear />
      <ModalLimite />
      <ModalEspecial />
      <ModalContestacao />
      <ModalPassword />
      <div className='creditcard--container'>
        <div className='view_profile--title'>
          <label htmlFor='creditcard--label'>
            <span>Cartões</span> <div className='container-icon'><i /> <i /> <i /></div>
          </label>
          <input type='checkbox' id='creditcard--label' />
          <div className='view_profile--menu'>
            <ul>
              <Can permission='add_card'>
                <li onClick={() => { changeModalAdicionar(true) }}>Adicionar cartão</li>
              </Can>
            </ul>
          </div>
        </div>
        <div className='creditcard--body'>
          {/* Titular */}
          {cards.map((_c, index) =>
            <div className={`creditcard creditcard--${index}`} key={index}>
              <label htmlFor={`creditcard--label${index + 50}`} className='creditcard--actions'>
                <i /> <i /> <i />
              </label>
              <input type='checkbox' id={`creditcard--label${index + 50}`} />
              <div className='credicard--actions--list'>
                <ul>
                  <Can permission='view_card'>
                    <li onClick={() => { changeModalInformacoes(true, index); setCardSelected(card => ({..._c, dependente: 0 }) ) }}>Informações</li>
                  </Can>
                  {!_c.bloqueado &&
                    <Can permission='block_card'>
                      <li onClick={() => { changeModalBloquear(true); setCardSelected(card => ({..._c, dependente: 0 }) ) }}>Bloquear</li>
                    </Can>}
                  {_c.bloqueado
                    ? <Can permission='unblock_card'>
                      <li onClick={() => { changeModalDesbloquear(true); setCardSelected(card => ({..._c, dependente: 0 }) ) }}>Desbloquear</li>
                      </Can> : null}
                  {/* <Can permission='second_via_card'>
                    <li onClick={() => { changeModalSegundaVia(true); setCardSelected(card => _c) }}>Segunda via</li>
                  </Can> */}
                  <Can permission='password_card'>
                    <li onClick={() => { changeModalAnuidade(true); setCardSelected(card => ({..._c, dependente: 0 }) ) }}>Anuidade</li>
                  </Can>
                  <Can permission='history_card'>
                    <li onClick={() => { changeModalHistorico(true); setCardSelected(card => ({..._c, dependente: 0 }) ); fillHistorico(_c) }}>Histórico</li>
                  </Can>
                  <Can permission='change_limit'>
                    <li onClick={() => { changeModalLimite(true); setCardSelected(card => ({..._c, dependente: 0 }) ) }}>Alterar Limite</li>
                  </Can>
                  <Can permission='special_request'>
                    <li onClick={() => { setCardSelected(card => ({..._c, dependente: 0 }) ); changeModalEspecial(true) }}> Soli. Especial</li>
                  </Can>
                  <Can permission='contestation'>
                    <li onClick={() => { setCardSelected(card => ({..._c, dependente: 0 }) ); changeModalContestacao(true) }}>Contestação</li>
                  </Can>
                  <Can permission='card_sms'>
                    <li onClick={() => { setCardSelected(card => ({..._c, dependente: 0 }) ); changeModalPassword(true) }}>Senha SMS</li>
                  </Can>
                </ul>
              </div>
              {_c.bandeira === 'MASTERCARD' &&
                <div className='creditcard--brand'>
                  <img src='https://brand.mastercard.com/content/dam/mccom/brandcenter/thumbnails/mastercard_vrt_rev_92px_2x.png' alt='logo operadora' />
                </div>}

              {_c.bandeira === 'VISA' &&
                <div className='creditcard--brand'>
                  <img src='https://notinostore.com.br/wp-content/uploads/2019/04/visa-5-logo-png-transparent.png' alt='logo operadora' />
                </div>}

              {_c.bandeira === 'ELO' &&
                <div className='creditcard--brand'>
                  <img src='https://www.elo.com.br/themes/custom/elo/img/logo_topo.png' alt='logo operadora' />
                </div>}

              <div className='creditcard--code'>
                <span>Código:</span>
                <span>{_c.id}</span>
              </div>
              <div className='creditcard--number'>
                <span>{_c.primeiroQuarto}</span>
                <span>XXXX</span>
                <span>XXXX</span>
                <span>{_c.segundoQuarto}</span>
              </div>
              <div className='creditcard--holder'>
                <span style={{ maxWidth: '90%' }}>{_c.nome}</span>
              </div>
              <div className='creditcard--expiry'>
                <span>Vencimento: </span>
                <span>{_c.diaVencimento}</span>
              </div>
              <div className='creditcard--limit'>
                <span>Limite: </span>
                <span>{`R$ ${_c.limite}`}</span>
              </div>
            </div>)}

            {/* dependente */}

            {cardsDependente.map((_c, index) =>
            <div className={`creditcard creditcard--${index + 1}`} key={index}>
              <label htmlFor={`creditcard--label${index + 100}`} className='creditcard--actions'>
                <i /> <i /> <i />
              </label>
              <input type='checkbox' id={`creditcard--label${index + 100}`} />
              <div className='credicard--actions--list'>
                <ul>
                  <Can permission='view_card'>
                    <li onClick={() => { changeModalInformacoes(true, index); setCardSelected(card => ({..._c, dependente: 1 }) ) }}>Informações</li>
                  </Can>
                  {!_c.bloqueado &&
                    <Can permission='block_card'>
                      <li onClick={() => { changeModalBloquear(true); setCardSelected(card => ({..._c, dependente: 1 }) ) }}>Bloquear</li>
                    </Can>}
                  {_c.bloqueado
                    ? <Can permission='unblock_card'>
                      <li onClick={() => { changeModalDesbloquear(true); setCardSelected(card => ({..._c, dependente: 1 }) ) }}>Desbloquear</li>
                      </Can> : null}
                  {/* <Can permission='second_via_card'>
                    <li onClick={() => { changeModalSegundaVia(true); setCardSelected(card => _c) }}>Segunda via</li>
                  </Can> */}
                  <Can permission='password_card'>
                    <li onClick={() => { changeModalAnuidade(true); setCardSelected(card => ({..._c, dependente: 1 }) ) }}>Anuidade</li>
                  </Can>
                  <Can permission='history_card'>
                    <li onClick={() => { changeModalHistorico(true); setCardSelected(card => ({..._c, dependente: 1 }) ); fillHistorico(_c) }}>Histórico</li>
                  </Can>
                  <Can permission='change_limit'>
                    <li onClick={() => { changeModalLimite(true); setCardSelected(card => ({..._c, dependente: 1 }) ) }}>Alterar Limite</li>
                  </Can>
                  <Can permission='special_request'>
                    <li onClick={() => { setCardSelected(card => ({..._c, dependente: 1 }) ); changeModalEspecial(true) }}> Soli. Especial</li>
                  </Can>
                  <Can permission='contestation'>
                    <li onClick={() => { setCardSelected(card => ({..._c, dependente: 1 }) ); changeModalContestacao(true) }}>Contestação</li>
                  </Can>
                  <Can permission='card_sms'>
                    <li onClick={() => { setCardSelected(card => ({..._c, dependente: 1 }) ); changeModalPassword(true) }}>Senha SMS</li>
                  </Can>
                </ul>
              </div>
              {_c.bandeira === 'MASTERCARD' &&
                <div className='creditcard--brand'>
                  <img src='https://brand.mastercard.com/content/dam/mccom/brandcenter/thumbnails/mastercard_vrt_rev_92px_2x.png' alt='logo operadora' />
                </div>}

              {_c.bandeira === 'VISA' &&
                <div className='creditcard--brand'>
                  <img src='https://notinostore.com.br/wp-content/uploads/2019/04/visa-5-logo-png-transparent.png' alt='logo operadora' />
                </div>}

              {_c.bandeira === 'ELO' &&
                <div className='creditcard--brand'>
                  <img src='https://www.elo.com.br/themes/custom/elo/img/logo_topo.png' alt='logo operadora' />
                </div>}

              <div className='creditcard--code'>
                <span>Código:</span>
                <span>{_c.id}</span>
              </div>
              <div className='creditcard--number'>
                <span>{_c.primeiroQuarto}</span>
                <span>XXXX</span>
                <span>XXXX</span>
                <span>{_c.segundoQuarto}</span>
              </div>
              <div className='creditcard--holder'>
                <span style={{ maxWidth: '90%' }}>{_c.nome}</span>
              </div>
              <div className='creditcard--expiry'>
                <span>Vencimento: </span>
                <span>{_c.diaVencimento}</span>
              </div>
              <div className='creditcard--limit'>
                <span>Limite: </span>
                <span>{`R$ ${_c.limite}`}</span>
              </div>
            </div>)}

        </div>
      </div>
    </>
  )
}

export default CreditCard
