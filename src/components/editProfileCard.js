import React, { useEffect, useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import Api from 'api'
import { NotificationManager } from 'react-notifications'
import Modal from 'react-responsive-modal'
import Input from 'components/input'
import Button from 'components/button'
import { changeModalProfile, fillClient } from 'store/actions/actionsClient'
import { Loader } from './loader'

function EditProfileClient () {
  const client = useSelector(state => state.clients.client)
  const [contacts, setContacts] = useState([])
  const [clientState, setClient] = useState([])
  const [modalContact, setModalContact] = useState(false)
  const [maskContact, setMaskContact] = useState('11111-1111')
  const [novoContato, setNovoContato] = useState({numero: '', ddd: ''})
  const [tipoContato, setTipoContato] = useState('CELULAR')
  const [loading, setLoading] = useState(false)

  const open = useSelector(state => state.clients.modal.profileModal.open)
  const edit = useSelector(state => state.clients.modal.profileModal.edit)
  const dispatch = useDispatch()

  try {
    useEffect(() => {
      async function fetchData () {
        const { contacts } = await Api.get('contatos', { id: client.id })
        console.log('contatos', contacts)
        setContacts(_c => contacts)
        setClient(_c => ({ ...client, address_residencial: { ...client.address_residencial, localidade: client.address_residencial.cidade } }))
        console.log(client)
      } fetchData()
    }, [client])
  } catch (error) {
    NotificationManager.error(error.message)
  }

  async function handleClient (event) {
    const { id, value } = event.target
    const field = id.split('-')[0]
    await setClient(_c => ({
      ..._c,
      [field]: value
    }))
  }

  function handleResidencial (event) {
    const { id, value } = event.target
    const field = id.split('_')[1]
    setClient(_c => ({
      ..._c,
      address_residencial: {
        ..._c.address_residencial,
        [field]: value.replace('-', '')
      }
    }))
  }

  function handleComercial (event) {
    const { id, value } = event.target
    const field = id.split('_')[1]
    setClient(_c => ({
      ..._c,
      address_comercial: {
        ..._c.address_comercial,
        [field]: value.replace('-', '')
      }
    }))
  }

  function handleOutro (event) {
    const { id, value } = event.target
    const field = id.split('_')[1]
    setClient(_c => ({
      ..._c,
      address_outro: {
        ..._c.address_outro,
        [field]: value.replace('-', '')
      }
    }))
  }

  function handleContact (event) {
    const { id, value } = event.target
    const field = id.split('-')[0]
    const newContact = contacts.map(_c => {
      if (_c.tipo.toLowerCase() === field) _c.numero = value
      return _c
    })
    setContacts(_c => newContact)
  }

  function closeModal () {
    dispatch(changeModalProfile({
      open: false,
      edit: false
    }))
  }

  async function save () {
    try {
      let dataParaApiCagada = {
        nome: clientState.name,
        email: clientState.email
      }

      console.log(clientState)

      if (clientState.address_comercial) {
        dataParaApiCagada = {
          ...dataParaApiCagada,
          enderecoComercial: {
            ...clientState.address_comercial,
            localidade: clientState.address_comercial ? clientState.address_comercial.localidade : null,
            cidade: clientState.address_comercial ? clientState.address_comercial.localidade : null
          }
        }
      }

      if (clientState.address_residencial) {
        dataParaApiCagada = {
          ...dataParaApiCagada,
          enderecoResidencial: {
            ...clientState.address_residencial,
            localidade: clientState.address_residencial ? clientState.address_residencial.localidade : null,
            cidade: clientState.address_residencial ? clientState.address_residencial.localidade : null
          }
        }
      }

      if (clientState.address_outro) {
        dataParaApiCagada = {
          ...dataParaApiCagada,
          enderecoAlternativo: {
            ...clientState.address_outro,
            localidade: clientState.address_outro ? clientState.address_outro.localidade : null,
            cidade: clientState.address_outro ? clientState.address_outro.localidade : null
          }
        }
      }

      await Api.put('client', {
        client: { ...dataParaApiCagada },
        id: client.id
      })

      console.log('aqui', {
        ...clientState,
        address_outro: dataParaApiCagada.enderecoAlternativo,
        address_comercial: dataParaApiCagada.enderecoComercial,
        address_residencial: dataParaApiCagada.enderecoResidencial
      })

      dispatch(fillClient({
        ...clientState,
        address_outro: dataParaApiCagada.enderecoAlternativo,
        address_comercial: dataParaApiCagada.enderecoComercial,
        address_residencial: dataParaApiCagada.enderecoResidencial
      }))
      NotificationManager.success('Atualizado com Sucesso')
      closeModal()
    } catch (error) {
      NotificationManager.error(error.message)
    }
  }

  function addContact() {
    setModalContact(true)
  }

  async function newContact() {
    setLoading(true)
    try {
      let resp = await Api.post('contatos', {...novoContato, id: clientState.id, tipoContato })
      setContacts(oldContact => {
        return [
          ...oldContact,
          resp.data
        ]
      })
      setModalContact(false)
      NotificationManager.success('Cadastrado com Sucesso')
    } catch (error) {
      NotificationManager.error(error.message)
    } finally {
      setLoading(false)
    }
  }

  function handleChangeTipoContato(event) {
    let mask = '11111-1111'
    if(event.target.value === 'RESIDENCIAL') mask = '1111-1111'
    setTipoContato(event.target.value)
    setMaskContact(mask)
  }

  function handleChangeContact(event) {
    const {id, value} = event.target
    let novoId = id.split('_')
    novoId = novoId[1]
    setNovoContato(prevCont => {
      return {
        ...prevCont,
        [novoId]: value
      }
    })
  }

  return (
    <>
      <Modal open={open} onClose={closeModal} center classNames={{ modal: 'profileModal' }}>
        <div className='profileModal--container'>
          <div className='profileModal--title'>
            <h1>Dados Cliente</h1>
          </div>
          <div className='profileModal--body'>
            <fieldset>
              <legend>Dados Pessoais</legend>
              <div className='row'>
                <Input type='text' label='Nome' id='name-client' value={clientState.name} disabled={!edit} onChange={handleClient} />
              </div>
              <div className='row'>
                <Input type='text' label='CPF' id='cpf-client' mask='111.111.111-11' value={clientState.cpf} disabled />
                <Input type='text' label='E-mail' id='email-client' disabled={!edit} value={clientState.email} onChange={handleClient} />
              </div>
              <div className='row'>
                <Input type='text' label='RG' id='rg-client' disabled={!edit} value={clientState.rg} onChange={handleClient} />
              </div>
            </fieldset>
            <fieldset>
              <legend>Contatos</legend>
              <span className='profileModal--body--addContact' onClick={addContact}>Adicionar contato +</span>
              {contacts.filter(_c => _c.tipo === 'CELULAR').length ?
                contacts.filter(_c => _c.tipo === 'CELULAR').map(contato => (
                    <Input disabled={true}  type='text' label='Celular' id='celular-client' mask='(11) 11111-1111' onChange={handleContact} value={contato.area + contato.numero} />
                  ))
                : null }

                {contacts.filter(_c => _c.tipo === 'RESIDENCIAL').length ?
                  contacts.filter(_c => _c.tipo === 'RESIDENCIAL').map(contato => (
                      <Input disabled={true}  type='text' label='residencial' id='residencial-client' mask='(11) 1111-1111' onChange={handleContact} value={contato.area + contato.numero} />
                    ))
                : null }

                {contacts.filter(_c => _c.tipo === 'COMERCIAL').length ?
                  contacts.filter(_c => _c.tipo === 'COMERCIAL').map(contato => (
                      <Input disabled={true}  type='text' label='comercial' id='comercial-client' mask='(11) 1111-1111' onChange={handleContact} value={contato.area + contato.numero} />
                    ))
                : null }
            </fieldset>
            <fieldset>
              <legend>Endereços </legend>
              {(client.address_residencial) &&
                <fieldset>
                  <legend>Residencial</legend>
                  <div className='row'>
                    <Input type='text' label='CEP' id='residencial_cep' disabled={!edit} mask='11111-111' onChange={handleResidencial} value={clientState.address_residencial ? clientState.address_residencial.cep : null} />
                    <Input type='text' label='Rua' id='residencial_logradouro' disabled={!edit} onChange={handleResidencial} value={clientState.address_residencial ? clientState.address_residencial.logradouro : null} />
                  </div>
                  <div className='row'>
                    <Input type='text' label='Nº' id='residencial_numero' disabled={!edit} onChange={handleResidencial} value={clientState.address_residencial ? clientState.address_residencial.numero : null} />
                    <Input type='text' label='Complemento' id='residencial_complemento' onChange={handleResidencial} disabled={!edit} value={clientState.address_residencial ? clientState.address_residencial.complemento : null} />
                    <Input type='text' label='Bairro' id='residencial_bairro' disabled={!edit} onChange={handleResidencial} value={clientState.address_residencial ? clientState.address_residencial.bairro : null} />
                  </div>
                  <div className='row'>
                    <Input type='text' label='Cidade' id='residencial_localidade' disabled={!edit} onChange={handleResidencial} value={clientState.address_residencial ? clientState.address_residencial.localidade : null} />
                    <Input type='text' label='UF' id='residencial_uf' disabled={!edit} onChange={handleResidencial} value={clientState.address_residencial ? clientState.address_residencial.uf : null} />
                  </div>
                </fieldset>}
              {(client.address_comercial) &&
                <fieldset>
                  <legend>Comercial</legend>
                  <div className='row'>
                    <Input type='text' label='CEP' id='comercial_cep' disabled={!edit} mask='11111-111' onChange={handleComercial} value={clientState.address_comercial ? clientState.address_comercial.cep : null} />
                    <Input type='text' label='Rua' id='comercial_logradouro' disabled={!edit} onChange={handleComercial} value={clientState.address_comercial ? clientState.address_comercial.logradouro : null} />
                  </div>
                  <div className='row'>
                    <Input type='text' label='Nº' id='comercial_numero' disabled={!edit} onChange={handleComercial} value={clientState.address_comercial ? clientState.address_comercial.numero : null} />
                    <Input type='text' label='Complemento' id='comercial_complemento' disabled={!edit} onChange={handleComercial} value={clientState.address_comercial ? clientState.address_comercial.complemento : null} />
                    <Input type='text' label='Bairro' id='comercial_bairro' disabled={!edit} onChange={handleComercial} value={clientState.address_comercial ? clientState.address_comercial.bairro : null} />
                  </div>
                  <div className='row'>
                    <Input type='text' label='Cidade' id='comercial_localidade' disabled={!edit} onChange={handleComercial} value={clientState.address_comercial ? clientState.address_comercial.localidade : null} />
                    <Input type='text' label='UF' id='comercial_uf' disabled={!edit} onChange={handleComercial} value={clientState.address_comercial ? clientState.address_comercial.uf : null} />
                  </div>
                </fieldset>}
              {(client.address_outro) &&
                <fieldset>
                  <legend>Outro</legend>
                  <div className='row'>
                    <Input type='text' label='CEP' id='outro_cep' disabled={!edit} onChange={handleOutro} mask='11111-111' value={clientState.address_outro ? clientState.address_outro.cep : null} />
                    <Input type='text' label='Rua' id='outro_logradouro' disabled={!edit} onChange={handleOutro} value={clientState.address_outro ? clientState.address_outro.logradouro : null} />
                  </div>
                  <div className='row'>
                    <Input type='text' label='Nº' id='outro_numero' disabled={!edit} onChange={handleOutro} value={clientState.address_outro ? clientState.address_outro.numero : null} />
                    <Input type='text' label='Complemento' id='outro_complemento' onChange={handleOutro} disabled={!edit} value={clientState.address_outro ? clientState.address_outro.complemento : null} />
                    <Input type='text' label='Bairro' id='outro_bairro' disabled={!edit} onChange={handleOutro} value={clientState.address_outro ? clientState.address_outro.bairro : null} />
                  </div>
                  <div className='row'>
                    <Input type='text' label='Cidade' id='outro_localidade' disabled={!edit} onChange={handleOutro} value={clientState.address_outro ? clientState.address_outro.localidade : null} />
                    <Input type='text' label='UF' id='outro_uf' disabled={!edit} onChange={handleOutro} value={clientState.address_outro ? clientState.address_outro.uf : null} />
                  </div>
                </fieldset>}
            </fieldset>
          </div>
          <div className='profileModal--footer'>
            {edit && <Button onClick={save}>Salvar</Button>}
          </div>
        </div>
      </Modal>
      <Modal open={modalContact} onClose={() => {setModalContact(false)}} center classNames={{ modal: 'contactModal' }}>
            <div className='contactModal--body'>
              <div className='contactModal--body--title'>
                <h1>Adicionar Contato</h1>
              </div>

              <div className='contactModal--body--select'>
                <label>Tipo</label>
                <select onChange={handleChangeTipoContato} value={tipoContato}>
                  <option value='CELULAR'>Celular</option>
                  <option value='RESIDENCIAL'>Residencial</option>
                  <option value='COMERCIAL'>Comercial</option>
                  <option value='REFERENCIA'>Referência</option>
                  <option value='OUTRO'>Outro</option>
                </select>
              </div>
              <div className='contactModal--body--tel'>
                <Input type='text' label='DDD' id='novo_ddd' mask='(11)' value={novoContato.ddd} onChange={handleChangeContact}/>
                <Input type='text' label='Número' id='novo_numero' mask={maskContact} value={novoContato.numero} onChange={handleChangeContact}/>
              </div>
              <div className='contactModal--body--salvar'>
                {loading ? <Loader /> : <Button onClick={newContact}>Salvar</Button> }
              </div>
            </div>
      </Modal>
    </>
  )
}

export default EditProfileClient
