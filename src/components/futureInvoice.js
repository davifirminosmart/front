import React from 'react'
import Modal from 'react-responsive-modal';
import Api from 'api'
import { NotificationManager } from 'react-notifications'
import Button from 'components/button'
import MaskedInput from 'react-maskedinput'
import { Loader } from 'components/loader'

class FutureInvoice extends React.Component {

  async componentDidMount() {
    const { clientData } = this.props
    const { data } = await Api.get(`invoices`, {id: clientData.id,
      type: 'FUTURA'})
    const rows = await data.map(fatura => {
      // fatura.document = clientData.cpf
      // fatura.description = 'Total Fatura Anterior'
      return fatura
    })
    this.setState({ rows })
  }

  state = {
    loading: false,
    openModalInvoice: false,
    openModalInvoiceSMS: false,
    openModalInvoiceEMAIL: false,
    rows: [],
    columns: [
      { name: 'Data', key: 'dataVencimento' },
      // { name: 'Documento', key: 'document' },
      // { name: 'Descrição', key: 'description' },
      { name: 'Valor (R$)', key: 'valorTotal' },
      { name: 'Valor Pago (R$)', key: 'valorPagamentosEfetuados' }
    ],
    detailedInformation: [],
    email: '',
    telephone: '',
    actualInvoiceId: null
  }

  handleEmail = event => {
    this.setState({ email: event.currentTarget.value })
  }

  handleTELEPHONE = event => {
    this.setState({ telephone: event.currentTarget.value })
  }

  sendEmail = async () => {
    try {
      // const { clientData } = this.props
      // const { actualInvoiceId, email } = this.state
      // await Api.post('sendEmail', {
      //   client_id: clientData.id,
      //   invoice_id: actualInvoiceId,
      //   email
      // })
    } catch(error) {
      return NotificationManager.error('FALHA AO ENVIAR E-MAIl')
    }
  }

  sendSMS = async () => {
    try {
      this.setState({ loading: true })
      const { clientData } = this.props
      let { actualInvoiceId, telephone } = this.state
      telephone = telephone.replace('(', '').replace(')', '').replace(' ', '').replace('-', '').replace('_', '')
      if(telephone.length != 11) throw new Error('ERRO_TELEFONE')
      await Api.post('sendSMS', {
        client_id: clientData.id,
        invoice_id: actualInvoiceId,
        telephone
      })
      this.setState({ loading: false, openModalInvoiceSMS: false })
      return NotificationManager.success('SMS Enviado')
    } catch(error) {
      this.setState({ loading: false })
      if(error.message == 'ERRO_TELEFONE') return NotificationManager.error('Informe o telefone')
      return NotificationManager.error('FALHA AO ENVIAR SMS')
    }
  }

  openModal = async event => {
    try {
      const { clientData } = this.props
      const { rows } = this.state
      const value = event.currentTarget.value
      const invoice_id = rows[value].idFatura
      const { data } = await Api.get(`detailedInvoice`, {
        client_id: clientData.id,
        invoice_id
      })
      this.setState({ openModalInvoice: true, detailedInformation: data,
        actualInvoiceId: invoice_id })
    } catch(error) {
      return NotificationManager.error('FALHA AO LISTAR DADOS')
    }
  }

  openModalSMS = async event => {
    try {
      const { rows } = this.state
      const value = event.currentTarget.value
      const invoice_id = rows[value].idFatura
      this.setState({ openModalInvoiceSMS: true, actualInvoiceId: invoice_id })
    } catch(error) {
      return NotificationManager.error('FALHA AO LISTAR DADOS')
    }
  }

  openModalInvoiceEMAIL = async event => {
    try {
      const { rows } = this.state
      const value = event.currentTarget.value
      const invoice_id = rows[value].idFatura
      this.setState({ openModalInvoiceEMAIL: true, actualInvoiceId: invoice_id })
    } catch(error) {
      NotificationManager.error('FALHA AO LISTAR DADOS')
    }
  }

  closeModalInvoice = () => {
    this.setState({ openModalInvoice: false })
  }

  closeModalInvoiceSMS = () => {
    this.setState({ openModalInvoiceSMS: false, loading: false })
  }

  closeModalInvoiceEMAIL = () => {
    this.setState({ openModalInvoiceEMAIL: false, loading: false })
  }

  clear = event => {
    const { rows } = this.state
    const value = event.currentTarget.value
    for(let i = 0; i < rows.length; i++) {
      if(i != value) {
        document.getElementById(`row_invoice${i}`).checked = false
      }
    }
  }

  render() {
    const { columns, rows, detailedInformation, email,
      telephone, loading } = this.state
    return (
      <>
        <div className='futureInvoice--container'>
          <div className='view_profile--title'>
            <label htmlFor='futureInvoice--label'>
              <span>Faturas Futuras</span>
              {/* <div className='container-icon'><i /> <i /> <i /></div> */}
            </label>
            {/* <input type='checkbox' id='invoice--label' />
            <div className='view_profile--menu'>
              <ul>
                <li>Sei la 1</li>
                <li>Sei la 2</li>
              </ul>
            </div> */}
          </div>
          <div className='backgrounfutureInvoice'>
            <table className='tablefutureInvoice'>
              <tr>
                {/* <th>Ações</th> */}
                {columns.map((column, key) => <th key={key} id={key}>{column.name}</th>)}
              </tr>
              {rows.map((row, index) => (
                <tr key={index}>
                  {/* <td>
                    <div className='view_profile--title'>
                      <label htmlFor={`row_invoice${index}`}>
                        <div className='container-icon'><i /> <i /> <i /></div>
                      </label>
                      <input type='checkbox' onClick={this.clear} id={`row_invoice${index}`} value={index}/>
                      <div className='subMenuTableInvoice'>
                        <ul>
                          <li onClickCapture={this.openModal} value={index}>Visualizar</li>
                          <li onClickCapture={this.openModalSMS} value={index}>Enviar SMS</li>
                          <li onClickCapture={this.openModalInvoiceEMAIL} value={index}>Enviar E-mail</li>
                        </ul>
                      </div>
                    </div>
                  </td> */}
                  {columns.map((column, key) => {
                    return <td key={key} id={key}>{row[column.key]}</td>
                  })}
                </tr>
              ))}
              {rows.length === 0 &&
                <tr>
                  <td>
                    <h3>Não foram encontrados dados de fatura</h3>
                  </td>
                </tr>}
            </table>
          </div>
        </div>
        <Modal
          open={this.state.openModalInvoice}
          onClose={this.closeModalInvoice} >
            <div className="informationBody">
                <div className="subInformation">
                  <div className="borderSpan">
                    <span>Informações sobre Fatura</span>
                  </div>
                  <div className="rowInformation">
                    <p>
                      <label>Número</label>
                      <input title={detailedInformation.idFatura}
                      style={{width: 105}}
                      type='text' disabled id='id_invoice'
                      value={detailedInformation.idFatura} />
                    </p>
                    <p>
                      <label>Status</label>
                      <input title={detailedInformation.status}
                      style={{width: 100}}
                      type='text' disabled id='status'
                      value={detailedInformation.status}/>
                    </p>
                    <p>
                      <label>Titular</label>
                      <input title={detailedInformation.nomeCartao}
                      style={{width: 240}} type='text' disabled id='card_owner'
                      value={detailedInformation.nomeCartao}/>
                    </p>
                    <p>
                      <label>Dia de Corte</label>
                      <input title={detailedInformation.diaDeCorte}
                      style={{width: 80}} type='text' disabled id='cut_day'
                      value={detailedInformation.diaDeCorte}/>
                    </p>
                    <p>
                      <label>Vencimento</label>
                      <input title={detailedInformation.dataVencimento}
                      style={{width: 110}} type='text' disabled id='expiry_date'
                      value={detailedInformation.dataVencimento}/>
                    </p>
                  </div>
                  <div className="rowInformation">
                    <p>
                      <label>Valor total</label>
                      <input title={detailedInformation.valorTotalFaturaAtual}
                      style={{width: 147}}
                      type='text' disabled id='total_value'
                      value={`R$ ${detailedInformation.valorTotalFaturaAtual}`} />
                    </p>
                    <p>
                      <label>Valor mínimo</label>
                      <input title={detailedInformation.valorPagamentoMinimoFaturaAtual}
                      style={{width: 107}}
                      type='text' disabled id='minimum_value'
                      value={`R$ ${detailedInformation.valorPagamentoMinimoFaturaAtual}`}/>
                    </p>
                    <p>
                      <label>Principal</label>
                      <input title={detailedInformation.totalPrincipalFatura}
                      style={{width: 137}} type='text' disabled id='main_invoice'
                      value={`R$ ${detailedInformation.totalPrincipalFatura}`}/>
                    </p>
                    <p>
                      <label>Tarifa</label>
                      <input title={detailedInformation.tarifaJurosRotativo}
                      style={{width: 117}} type='text' disabled id='tax'
                      value={`R$ ${detailedInformation.tarifaJurosRotativo}`}/>
                    </p>
                    <p>
                      <label>Encargos</label>
                      <input title={detailedInformation.totalEncargosFatura}
                      style={{width: 127}} type='text' disabled id='charges'
                      value={`R$ ${detailedInformation.totalEncargosFatura}`}/>
                    </p>
                  </div>
                </div>
                <div className="subInformation">
                  <div className="borderSpan">
                    <span>Saldo do Cartão</span>
                  </div>
                  <div className="rowInformationDebt">
                    <p>
                      <label>Limite do Crédito</label>
                      <input title={detailedInformation.limiteRotativo}
                      style={{width: 98.75}}
                      type='text' disabled id='credit_limit'
                      value={`R$ ${detailedInformation.limiteRotativo}`} />
                      <text>-</text>
                    </p>
                    <p>
                      <label>Devedor</label>
                      <input title={detailedInformation.devedorRotativo}
                      style={{width: 98.75}}
                      type='text' disabled id='debt_balance'
                      value={`R$ ${detailedInformation.devedorRotativo}`}/>
                      <text>-</text>
                    </p>
                    <p>
                      <label>Pré-autorizado</label>
                      <input title={detailedInformation.saldoDisponivelRotativo}
                      style={{width: 98.75}} type='text' disabled
                      id='pre_authorized_balance'
                      value={`R$ ${detailedInformation.saldoDisponivelRotativo}`}/>
                      <text className="equalSignal">=</text>
                    </p>
                    <p>
                      <label>Disponível</label>
                      <input title={detailedInformation.saldoDisponivelParcelado}
                      style={{width: 98.75}} type='text' disabled id='available'
                      value={`R$ ${detailedInformation.saldoDisponivelParcelado}`}/>
                    </p>
                  </div>
                </div>
            </div>
        </Modal>
        <Modal
          open={this.state.openModalInvoiceSMS}
          onClose={this.closeModalInvoiceSMS}>
            <div className="sendBody">
                <div className="sendLabel">
                  <label>Por favor digite o Telefone que deseja enviar a fatura:</label>
                </div>
                <div className="sendInput">
                  <p>
                    <MaskedInput id='telephone'
                    mask="(11) 11111-1111"
                    value={telephone}
                    className="inputStyle"
                    onChange={this.handleTELEPHONE}/>
                  </p>
                </div>
                {!loading &&
                  <div className="sendButtons">
                      <Button onClick={this.closeModalInvoiceSMS}>Cancelar</Button>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      <Button onClick={this.sendSMS}>Confirmar</Button>
                  </div>
                }
                {loading &&
                  <div className="sendButtons">
                    <Loader />
                  </div>
                }
            </div>
        </Modal>
        <Modal
          open={this.state.openModalInvoiceEMAIL}
          onClose={this.closeModalInvoiceEMAIL}>
            <div className="sendBody">
                <div className="sendLabel">
                  <label>Por favor digite o E-mail que deseja enviar a fatura:</label>
                </div>
                <div className="sendInput">
                  <p>
                    <input type='text' id='email'
                    value={email}
                    className="inputStyle"
                    onChange={this.handleEmail}/>
                  </p>
                </div>
                <div className="sendButtons">
                  <Button onClick={this.closeModalInvoiceEMAIL}>Cancelar</Button>
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <Button onClick={this.sendEmail}>Confirmar</Button>
                </div>
            </div>
        </Modal>
        {/* <Modal
          open={this.state.openModalInvoice}
          onClose={this.closeModalInvoice} >
          <div className="headerBoleto">
              Fatura - 9988110022
          </div>
          <div className="valuesBoleto">
              <div className="square">
                <p>vencimento</p>
                <span>05/04/2014</span>
              </div>
              <div className="square">
                <p>Pagamento Total</p>
                <span>R$ 1.308,64</span>
              </div>
              <div className="square">
                <p>Pagamento mínimo</p>
                <span>R$ 196,29</span>
              </div>
              <div className="square">
                <p>Pagamento da fatura</p>
                <span>R$ 24x 85,15</span>
              </div>
          </div>
          <div className="textBoleto">
                <div className="leftTextBoleto">
                  <div className="data">
                      <div className="borderSpan">
                        <span>Limite de crédito R$</span>
                      </div>
                      <div>
                        <div className="dataInformation">
                          <p>Limite total de crédito</p>
                          <p className="valueText">9.500,00</p>
                        </div>
                        <div className="dataInformation">
                          <p>Retirada de recursos País(saque)</p>
                          <p className="valueText">1.000,00</p>
                        </div>
                        <div className="dataInformation">
                          <p>Retirada de recursos Exterior(saque)</p>
                          <p className="valueText">7.000,00</p>
                        </div>
                      </div>
                  </div>
                  <div className="data">
                      <div className="borderSpan">
                        <span>Encargos desta fatura entre&#013; 05/03/14 e 04/04/14</span>
                      </div>
                      <div>
                        <div className="dataInformation">
                          <p>Juros de financiamento</p>
                          <p className="valueText">10.23%</p>
                        </div>
                        <div className="dataInformation">
                          <p>Juros de Mora</p>
                          <p className="valueText">1.00%</p>
                        </div>
                        <div className="dataInformation">
                          <p>Multa por atraso</p>
                          <p className="valueText">2.00%</p>
                        </div>
                        <div className="dataInformation">
                          <p>IOF de financiamento</p>
                          <p className="valueText">R$ 0.00</p>
                        </div>
                      </div>
                  </div>
                  <div className="data">
                      <div className="borderSpan">
                        <span>Fique atento aos encargos para o próximo período
                        (06/05/14)
                        </span>
                      </div>
                      <div>
                        <div className="dataInformation">
                          <p>Juros máximos do contrato</p>
                          <p className="valueText">13.90% ~ 367.20%</p>
                        </div>
                      </div>
                  </div>
                  <div className="data">
                      <div className="borderSpan">
                        <span>Financiamento da fatura</span>
                      </div>
                      <div>
                        <div className="dataInformation">
                          <p>Valor da fatura atual</p>
                          <p className="valueText">R$ 1.308,64</p>
                        </div>
                        <div className="dataInformation">
                          <p>Juros de financiamento</p>
                          <p className="valueText">13.90% a.m 367.20% a.a</p>
                        </div>
                      </div>
                  </div>
                </div>
                <div className="rightTextBoleto">
                  <div className="data">
                      <div className="borderSpan">
                        <span>Compras parceladas com juros</span>
                      </div>
                      <div>
                        <div className="dataInformation">
                          <p>Limite de crédito</p>
                          <p className="valueText">R$ 9.500,00</p>
                        </div>
                        <div className="dataInformation">
                          <p>Juros da compra parcelada</p>
                          <p className="valueText">3.90% ~ 59.28%</p>
                        </div>
                        <div className="dataInformation">
                          <p>CET da compra parcelada</p>
                          <p className="valueText">4.05% ~ 62.09%</p>
                        </div>
                        <div className="dataInformation">
                          <p>Valor total financiado</p>
                          <p className="valueText">R$ 9.500,00(100.00%)</p>
                        </div>
                        <div className="dataInformation">
                          <p>Valor do IOF</p>
                          <p className="valueText">R$ 155,19</p>
                        </div>
                        <div className="dataInformation">
                          <p>Valor total à pagar</p>
                          <p className="valueText">R$ 14.801,04</p>
                        </div>
                      </div>
                  </div>
                  <div className="data">
                      <div className="borderSpan">
                        <span>Demais Taxas de Juro</span>
                      </div>
                      <div>
                        <div className="dataInformation">
                          <p>De retirada de recursos país</p>
                          <p className="valueText">13.90%</p>
                        </div>
                        <div className="dataInformation">
                          <p>De pagamento de contas</p>
                          <p className="valueText">2.99%</p>
                        </div>
                        <div className="dataInformation">
                          <p>De pagamento de contas aut.</p>
                          <p className="valueText">2.99%</p>
                        </div>
                      </div>
                  </div>
                </div>
          </div>
        </Modal> */}
      </>
    )
  }
}

export default FutureInvoice
