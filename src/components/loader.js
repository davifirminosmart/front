import React from 'react'

export function Loader () {
  return (
    <>
      <div className='ring1'>
        <div className='ring2'>
          <div className='ring3'>
            <div className='ring4' />
          </div>
        </div>
      </div>
    </>
  )
}

export function VerticalLoader () {
  return (
    <div className='newtons-cradle'>
      <div />
      <div />
      <div />
    </div>
  )
}
