import React from 'react'
import Api from 'api'
import Modal from 'react-responsive-modal'
import { NotificationManager } from 'react-notifications'
import Button from 'components/button'
import MaskedInput from 'react-maskedinput'
import { Loader } from 'components/loader'
import Can from 'layout/can'

class LastPurchase extends React.Component {
  async componentDidMount () {
    const { clientData } = this.props
    const { data } = await Api.get(`lastPurchases/${clientData.id}`)
    const rows = data.map(fatura => {
      switch (fatura.status) {
        case 'PASSADA':
          fatura.status = <i title='PASSADA' className='lni lni-dollar' style={{ fontSize: '1.5em', color: 'red' }} />
          break
        case 'ATUAL':
          fatura.status = <i title='ATUAL' className='lni lni-dollar' style={{ fontSize: '1.5em', color: 'yellow' }} />
        case 'FUTURA':
          fatura.status = <i title='FUTURA' alt='Futura' className='lni lni-dollar' style={{ fontSize: '1.5em', color: 'green' }} />
        default:
          fatura.status = <i title='FUTURA' alt='Futura' className='lni lni-dollar' style={{ fontSize: '1.5em', color: 'green' }} />
      }
      // fatura.establishment = 'Não Definido'
      return fatura
    })
    this.setState({ rows })
  }

  state = {
    openModalLastPurchase: false,
    openModalLastPurchaseSMS: false,
    openModalLastPurchaseEMAIL: false,
    loading: false,
    columns: [
      { name: 'Status', key: 'status' },
      { name: 'Data', key: 'dataVencimento' },
      // { name: 'Estabelecimento', key: 'establishment' },
      { name: 'Valor (R$)', key: 'valorTotal' },
      { name: 'Valor Pago (R$)', key: 'valorPagamentosEfetuados' }
    ],
    columnsLancamentos: [
      { name: 'Data Lançamento', key: 'dataLancamento' },
      { name: 'Descrição Lançamento', key: 'descricaoLancamento' },
      { name: 'Valor', key: 'valor' },
      { name: 'Tipo', key: 'tipo' },
      { name: 'Data Transação', key: 'dataTransacao' }
    ],
    rows: [],
    rowsLancamentos: [],
    detailedInformation: [],
    email: '',
    telephone: '',
    actualInvoiceId: null
  }

  handleEmail = event => {
    this.setState({ email: event.currentTarget.value })
  }

  handleTELEPHONE = event => {
    this.setState({ telephone: event.currentTarget.value })
  }

  sendEmail = async () => {
    try {
      this.setState({ loading: true })
      const { clientData, protocol } = this.props
      const { actualInvoiceId, email } = this.state
      if (!email) throw new Error('ERRO_EMAIL')
      await Api.post('sendEmail', {
        client_id: clientData.id,
        invoice_id: actualInvoiceId,
        email,
        protocol,
        clientData
      })
      this.setState({ loading: false, openModalLastPurchaseEMAIL: false })
      return NotificationManager.success('E-mail Enviado')
    } catch (error) {
      this.setState({ loading: false })
      if (error.message == 'ERRO_EMAIL') return NotificationManager.error('Informe o E-mail')
      if(error.message == 'Falha ao gerar Boleto') return NotificationManager.error(error.message)
      return NotificationManager.error('Falha ao enviar E-mail')
    }
  }

  sendSMS = async () => {
    try {
      this.setState({ loading: true })
      const { clientData, protocol } = this.props
      let { actualInvoiceId, telephone } = this.state
      telephone = telephone.replace('(', '').replace(')', '').replace(' ', '').replace('-', '').replace('_', '')
      if (telephone.length != 11) throw new Error('ERRO_TELEFONE')
      await Api.post('sendSMS', {
        client_id: clientData.id,
        invoice_id: actualInvoiceId,
        telephone,
        protocol,
        clientData
      })
      this.setState({ loading: false, openModalLastPurchaseSMS: false })
      return NotificationManager.success('SMS Enviado')
    } catch (error) {
      this.setState({ loading: false })
      if (error.message == 'ERRO_TELEFONE') return NotificationManager.error('Informe o telefone')
      if(error.message == 'Falha ao gerar Boleto') return NotificationManager.error(error.message)
      return NotificationManager.error('Falha ao enviar SMS')
    }
  }

  openModal = async event => {
    try {
      const { clientData } = this.props
      const { rows } = this.state
      const value = event.currentTarget.value
      const invoice_id = rows[value].idFatura
      if (invoice_id == '[Futura]') throw new Error('ERRO_INVOICE_ID')
      const { data } = await Api.get('detailedInvoice', {
        client_id: clientData.id,
        invoice_id
      })
      this.setState({
        openModalLastPurchase: true,
        detailedInformation: data,
        actualInvoiceId: invoice_id,
        rowsLancamentos: data.lancamentos
      })
    } catch (error) {
      if (error.message == 'ERRO_INVOICE_ID') return NotificationManager.error('Não é possível listar o detalhe dessa fatura')
      return NotificationManager.error('FALHA AO LISTAR DADOS')
    }
  }

  openModalSMS = async event => {
    try {
      const { rows } = this.state
      const value = event.currentTarget.value
      const invoice_id = rows[value].idFatura
      this.setState({ openModalLastPurchaseSMS: true, actualInvoiceId: invoice_id })
    } catch (error) {
      NotificationManager.error('FALHA AO LISTAR DADOS')
    }
  }

  openModalEMAIL = async event => {
    try {
      const { rows } = this.state
      const value = event.currentTarget.value
      const invoice_id = rows[value].idFatura
      this.setState({ openModalLastPurchaseEMAIL: true, actualInvoiceId: invoice_id })
    } catch (error) {
      NotificationManager.error('FALHA AO LISTAR DADOS')
    }
  }

  closeModalLastPurchase = () => {
    this.setState({ openModalLastPurchase: false })
  }

  closeModalLastPurchaseSMS = () => {
    this.setState({ openModalLastPurchaseSMS: false, loading: false })
  }

  closeModalLastPurchaseEMAIL = () => {
    this.setState({ openModalLastPurchaseEMAIL: false, loading: false })
  }

  clear = event => {
    const { rows } = this.state
    const value = event.currentTarget.value
    for (let i = 0; i < rows.length; i++) {
      if (i != value) {
        if (rows[i].idFatura != '[Futura]') {
          document.getElementById(`row_lastPurchase_${i}`).checked = false
        }
      }
    }
  }

  render () {
    const {
      columns, rows, detailedInformation, email,
      telephone, loading, rowsLancamentos, columnsLancamentos
    } = this.state
    return (
      <>
        <div className='lastPurchase--container'>
          <div className='view_profile--title'>
            <label htmlFor='lastPurchase--label'>
              <span>Últimas Compras</span>
              {/* <div className='container-icon'><i /> <i /> <i /></div> */}
            </label>
            {/* <input type='checkbox' id='lastPurchase--label' />
            <div className='view_profile--menu'>
              <ul>
                <li>Sei la 1</li>
                <li>Sei la 2</li>
              </ul>
            </div> */}
          </div>
          <div className='backgroundLastPurchase'>
            <table className='tableLastPurchase'>
              <tr>
                {columns.map((column, key) => <th key={key} id={key}>{column.name}</th>)}
              </tr>
              {rows.map((row, index) => (
                <tr key={index}>
                  {columns.map((column, key) => {
                    return <td key={key} id={key}>{row[column.key]}</td>
                  })}
                </tr>
              ))}
              {rows.length === 0 &&
                <tr>
                  <td colSpan='100%'>
                    <h3>Não foram encontrados dados de fatura</h3>
                  </td>
                </tr>}
            </table>
          </div>
        </div>

        <Modal
          open={this.state.openModalLastPurchase}
          onClose={this.closeModalLastPurchase}
        >
          <div className='informationBody'>
            <div className='subInformation'>
              <div className='borderSpan'>
                <span>Informações sobre Fatura</span>
              </div>
              <div className='rowInformation'>
                <p>
                  <label>Número</label>
                  <input
                    title={detailedInformation.idFatura}
                    style={{ width: 105 }}
                    type='text' disabled id='id_invoice'
                    value={detailedInformation.idFatura}
                  />
                </p>
                <p>
                  <label>Status</label>
                  <input
                    title={detailedInformation.status}
                    style={{ width: 100 }}
                    type='text' disabled id='status'
                    value={detailedInformation.status}
                  />
                </p>
                <p>
                  <label>Titular</label>
                  <input
                    title={detailedInformation.nomeCartao}
                    style={{ width: 240 }} type='text' disabled id='card_owner'
                    value={detailedInformation.nomeCartao}
                  />
                </p>
                <p>
                  <label>Dia de Corte</label>
                  <input
                    title={detailedInformation.diaDeCorte}
                    style={{ width: 80 }} type='text' disabled id='cut_day'
                    value={detailedInformation.diaDeCorte}
                  />
                </p>
                <p>
                  <label>Vencimento</label>
                  <input
                    title={detailedInformation.dataVencimento}
                    style={{ width: 110 }} type='text' disabled id='expiry_date'
                    value={detailedInformation.dataVencimento}
                  />
                </p>
              </div>
              <div className='rowInformation'>
                <p>
                  <label>Valor total</label>
                  <input
                    title={detailedInformation.valorTotalFaturaAtual}
                    style={{ width: 147 }}
                    type='text' disabled id='total_value'
                    value={`R$ ${detailedInformation.valorTotalFaturaAtual}`}
                  />
                </p>
                <p>
                  <label>Valor mínimo</label>
                  <input
                    title={detailedInformation.valorPagamentoMinimoFaturaAtual}
                    style={{ width: 107 }}
                    type='text' disabled id='minimum_value'
                    value={`R$ ${detailedInformation.valorPagamentoMinimoFaturaAtual}`}
                  />
                </p>
                <p>
                  <label>Principal</label>
                  <input
                    title={detailedInformation.totalPrincipalFatura}
                    style={{ width: 137 }} type='text' disabled id='main_invoice'
                    value={`R$ ${detailedInformation.totalPrincipalFatura}`}
                  />
                </p>
                <p>
                  <label>Tarifa</label>
                  <input
                    title={detailedInformation.tarifaJurosRotativo}
                    style={{ width: 117 }} type='text' disabled id='tax'
                    value={`R$ ${detailedInformation.tarifaJurosRotativo}`}
                  />
                </p>
                <p>
                  <label>Encargos</label>
                  <input
                    title={detailedInformation.totalEncargosFatura}
                    style={{ width: 127 }} type='text' disabled id='charges'
                    value={`R$ ${detailedInformation.totalEncargosFatura}`}
                  />
                </p>
              </div>
            </div>
            <div className='subInformation'>
              <div className='borderSpan'>
                <span>Saldo do Cartão</span>
              </div>
              <div className='rowInformationDebt'>
                <p>
                  <label>Crédito</label>
                  <input
                    title={detailedInformation.limiteRotativo}
                    style={{ width: 98.75 }}
                    type='text' disabled id='credit_limit'
                    value={`R$ ${detailedInformation.limiteRotativo}`}
                  />
                  <text>-</text>
                </p>
                <p>
                  <label>Devedor</label>
                  <input
                    title={detailedInformation.devedorRotativo}
                    style={{ width: 98.75 }}
                    type='text' disabled id='debt_balance'
                    value={`R$ ${detailedInformation.devedorRotativo}`}
                  />
                  <text>-</text>
                </p>
                <p>
                  <label>Pré-autorizado</label>
                  <input
                    title={detailedInformation.saldoDisponivelRotativo}
                    style={{ width: 98.75 }} type='text' disabled
                    id='pre_authorized_balance'
                    value={`R$ ${detailedInformation.saldoDisponivelRotativo}`}
                  />
                  <text className='equalSignal'>=</text>
                </p>
                <p>
                  <label>Disponível</label>
                  <input
                    title={detailedInformation.saldoDisponivelParcelado}
                    style={{ width: 98.75 }} type='text' disabled id='available'
                    value={`R$ ${detailedInformation.saldoDisponivelParcelado}`}
                  />
                </p>
              </div>
            </div>
            <div className='subInformation'>
              <div className='borderSpan'>
                <span>Lançamentos</span>
              </div>
              <div className='backgrounInvoice'>
                <table className='tableInvoice'>
                  <tr>
                    {columnsLancamentos.map((column, key) => <th key={key} id={key}>{column.name}</th>)}
                  </tr>
                  {rowsLancamentos.map((row, index) => (
                    <tr key={index}>
                      {columnsLancamentos.map((column, key) => {
                        return <td key={key} id={key}>{row[column.key]}</td>
                      })}
                    </tr>
                  ))}
                  {rowsLancamentos.length === 0 &&
                    <tr>
                      <td>
                        <h3 className='noResponse'>Não foram encontrados lançamentos dessa fatura.</h3>
                      </td>
                    </tr>}
                </table>
              </div>
            </div>
          </div>
        </Modal>

        <Modal
          open={this.state.openModalLastPurchaseSMS}
          onClose={this.closeModalLastPurchaseSMS}
        >
          <div className='sendBody'>
            <div className='sendLabel'>
              <label>Por favor digite o Telefone que deseja enviar a fatura:</label>
            </div>
            <div className='sendInput'>
              <p>
                <MaskedInput
                  id='telephone'
                  mask='(11) 11111-1111'
                  value={telephone}
                  className='inputStyle'
                  onChange={this.handleTELEPHONE}
                />
              </p>
            </div>
            {!loading &&
              <div className='sendButtons'>
                <Button onClick={this.closeModalLastPurchaseSMS}>Cancelar</Button>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <Button onClick={this.sendSMS}>Confirmar</Button>
              </div>}
            {loading &&
              <div className='sendButtons'>
                <Loader />
              </div>}
          </div>
        </Modal>
        <Modal
          open={this.state.openModalLastPurchaseEMAIL}
          onClose={this.closeModalLastPurchaseEMAIL}
        >
          <div className='sendBody'>
            <div className='sendLabel'>
              <label>Por favor digite o E-mail que deseja enviar a fatura:</label>
            </div>
            <div className='sendInput'>
              <p>
                <input
                  type='text' id='email'
                  value={email}
                  className='inputStyle'
                  onChange={this.handleEmail}
                />
              </p>
            </div>
            {!loading &&
              <div className='sendButtons'>
                <Button onClick={this.closeModalLastPurchaseEMAIL}>Cancelar</Button>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <Button onClick={this.sendEmail}>Confirmar</Button>
              </div>}
            {loading &&
              <div className='sendButtons'>
                <Loader />
              </div>}
          </div>
        </Modal>
      </>
    )
  }
}

export default LastPurchase
