import React, { useState } from 'react'
import { NotificationManager } from 'react-notifications'
import Api from 'api'
import Modal from 'react-responsive-modal'
import Input from 'components/input'
import Button from 'components/button'
import { addDependentes } from '../store/actions/actionsDependente'
import { fillCardDependente } from '../store/actions/actionsCardDependente'
import { Loader } from 'components/loader'
import { useSelector, useDispatch } from 'react-redux'

function ModalDependente (props) {
  const [titular, setTitular] = useState({ nome: '', grauParentesco: 'PAI', cpf: '', dataNascimento: '', rg: '', sexo: 'MASCULINO' })
  const [loading, setLoading] = useState(false)
  const [contatos, setContatos] = useState([{ id: 1, faturaDigital: false, receberSMS: false, tipo: 'RESIDENCIAL' }])
  const [enderecos, setEnderecos] = useState([{ id: 1, pais: 'Brasil', cep: '', logradouro: '', numero: '', complemento: '', bairro: '', localidade: '', uf: '' }])
  const cardsDependente = useSelector(state => state.cardsDepentende.cards)
  const dependentes = useSelector(state => state.dependentes.dependentes)
  const dispatch = useDispatch()

  function changeTitular (field, event) {
    event.persist()
    try {
      if (event.target) setTitular(_t => ({ ..._t, [field]: event.target.value }))
    } catch (error) {
    }
  }

  async function save () {
    setLoading(_l => true)
    try {
      const endFormatado = enderecos.map(end => {
        end.numero = Number(end.numero)
        end.cep = Number(end.cep)
        return end
      })
      const telFormatado = contatos.map(tel => {
        tel.telefone = Number(tel.telefone)
        tel.area = Number(tel.area)
        return tel
      })

      let formatedData = titular.dataNascimento.split('/')
      formatedData = `${formatedData[2]}-${formatedData[1]}-${formatedData[0]}`
      const adicional = await Api.post('adicional', {
        titular: {
          ...titular,
          idPessoa: props.client.id,
          dataNascimento: formatedData,
          cpf: Number(titular.cpf.split('.').join('').replace('-', '')),
          rg: Number(titular.rg.split('.').join('').replace('-', '')),
          grauParentesco: titular.grauParentesco,
          endereco: endFormatado,
          telefones: telFormatado
        }
      })
      NotificationManager.success('Adicionado com Sucesso')
      const novoDependente = {
        id: adicional.dependente ? adicional.dependente.id : null,
        nome: titular.nome,
        cpf: titular.cpf,
        sexo: titular.sexo,
        nascimento: titular.dataNascimento
      }
      dispatch(fillCardDependente([]))
      dispatch(addDependentes(novoDependente))
      setLoading(_l => false)
    } catch (error) {
      NotificationManager.error(error.message)
      setLoading(_l => false)
    }
  }

  function handleContato (event, contatoId) {
    const { value, id } = event.target
    const key = id.split('-')[0]
    const novosContatos = contatos.map(contato => {
      if (contato.id === contatoId) {
        return {
          ...contato,
          [key]: value.replace('(', '')
            .replace(')', '')
            .replace(' ', '')
            .replace('-', '')
        }
      }
      return contato
    })
    console.log(novosContatos)
    setContatos(novosContatos)
  }

  async function novoContato () {
    const ultimoId = contatos[contatos.length - 1].id
    const novosContato = contatos.concat([{ id: ultimoId + 1, faturaDigital: false, receberSMS: false, tipo: 'RESIDENCIAL' }])
    await setContatos(novosContato)
  }

  function handleEndereco (event, enderecoId) {
    const { value, id } = event.target
    const key = id.split('-')[0]
    const novosEndereco = enderecos.map(endereco => {
      if (endereco.id === enderecoId) {
        return {
          ...endereco,
          [key]: value.replace('-', '')
        }
      }
      return endereco
    })
    setEnderecos(novosEndereco)
  }

  async function novoEndereco () {
    const ultimoId = enderecos[enderecos.length - 1].id
    const novosEnderecos = enderecos.concat([{ id: ultimoId + 1, pais: 'Brasil' }])
    await setEnderecos(novosEnderecos)
  }

  return (
    <>
      <Modal open={props.open} onClose={() => { props.close(false) }} center classNames={{ modal: 'modalAdicionar' }}>
        <fieldset>
          <legend>Dados Pessoais</legend>
          <div className='row'>
            <Input type='text' label='Nome' id='name-client' onChange={(event) => { changeTitular('nome', event) }} value={String(titular.nome)} />
          </div>
          <div className='row'>
            <Input type='text' label='CPF' mask='111.111.111-11' onChange={(event) => { changeTitular('cpf', event) }} value={String(titular.cpf)} />
            <Input type='text' label='Nascimento' mask='11/11/1111' onChange={(event) => { changeTitular('dataNascimento', event) }} value={String(titular.dataNascimento)} />
            <Input type='text' label='RG' onChange={(event) => { changeTitular('rg', event) }} value={String(titular.rg)} />
          </div>
          <div className='row row-select'>
            <label>Sexo</label>
            <select onChange={(event) => { changeTitular('sexo', event) }} value={String(titular.sexo)}>
              <option value='MASCULINO'>Masculino</option>
              <option value='FEMININO'>Feminino</option>
            </select>
          </div>
          <div className='row row-select'>
            <label>Parentesco</label>
            <select onChange={(event) => { changeTitular('grauParentesco', event) }} value={String(titular.grauParentesco)}>
              <option value='PAI'>Pai</option>
              <option value='MAE'>Mãe</option>
              <option value='FILHO'>Filho</option>
              <option value='IRMAO'>Irmão</option>
              <option value='CONJUGE'>Conjuge</option>
              <option value='OUTROS'>Outro</option>
            </select>
          </div>
        </fieldset>
        <fieldset>
          <legend>Contato</legend>
          <div className='row row-new'>
            <span onClick={novoContato}>+ novo contato</span>
          </div>
          {contatos.map(contato => {
            return <div style={{ display: 'flex', flexDirection: 'column', marginTop: '10px' }}>
              <div className='row row-select'>
                <label>Tipo de contato</label>
                <select
                  id={`tipo-${contato.id}`}
                  onChange={(event) => { handleContato(event, contato.id) }} value={null || String(contato.tipo)}
                >
                  <option value='RESIDENCIAL'>Residencial</option>
                  <option value='COMERCIAL'>Comercial</option>
                  <option value='CELULAR'>Celular</option>
                  <option value='REFERENCIA'>Referência</option>
                  <option value='OUTRO'>Outro</option>
                </select>
              </div>
              <div className='row'>
                <div style={{ flex: 1, marginRight: '10px' }}>
                  <Input
                    type='text' id={`area-${contato.id}`}
                    label='DDD' mask='(11)'
                    onChange={(event) => { handleContato(event, contato.id) }} value={null || String(contato.area)}
                  />
                </div>
                <div style={{ flex: 20 }}>
                  <Input
                    type='text' id={`telefone-${contato.id}`} label='Número'
                    mask='11111-1111' onChange={(event) => { handleContato(event, contato.id) }}
                    value={null || String(contato.telefone)}
                  />
                </div>
              </div>
                   </div>
          })}
        </fieldset>
        <fieldset>
          <legend>Endereço</legend>
          {enderecos.map(endereco => (
            <div style={{ display: 'flex', flexDirection: 'column', marginTop: '10px' }}>
              <div className='row'>
                <div style={{ flex: 1, marginRight: '10px' }}>
                  <Input
                    type='text' label='CEP' mask='11111-111'
                    id={`cep-${endereco.id}`}
                    onChange={(event) => { handleEndereco(event, endereco.id) }} value={null || String(endereco.cep)}
                  />
                </div>
                <div style={{ flex: 7 }}>
                  <Input
                    type='text' label='Rua'
                    id={`logradouro-${endereco.id}`}
                    onChange={(event) => { handleEndereco(event, endereco.id) }} value={null || String(endereco.logradouro)}
                  />
                </div>
              </div>
              <div className='row'>
                <Input
                  type='text'
                  id={`numero-${endereco.id}`}
                  label='Número'
                  onChange={(event) => { handleEndereco(event, endereco.id) }}
                  value={null || String(endereco.numero)}
                />

                <Input
                  type='text' label='Complemento'
                  id={`complemento-${endereco.id}`}
                  onChange={(event) => { handleEndereco(event, endereco.id) }}
                  value={null || String(endereco.complemento)}
                />

                <Input
                  type='text' label='Bairro'
                  id={`bairro-${endereco.id}`}
                  onChange={(event) => { handleEndereco(event, endereco.id) }}
                  value={null || String(endereco.bairro)}
                />

                <Input
                  type='text' label='Cidade'
                  id={`localidade-${endereco.id}`}
                  onChange={(event) => { handleEndereco(event, endereco.id) }}
                  value={null || String(endereco.localidade)}
                />

                <Input
                  type='text' id={`uf-${endereco.id}`}
                  label='UF' onChange={(event) => { handleEndereco(event, endereco.id) }}
                  value={String(endereco.uf)}
                />
              </div>
            </div>))}
        </fieldset>
        <div className='row row-button'>
          {!loading && <Button onClick={save}>Salvar</Button>}
          {loading && <Loader />}
        </div>
      </Modal>
    </>
  )
}

export default ModalDependente
