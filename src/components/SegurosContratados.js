import React from 'react'

function SegurosContratados() {
  return (
  <>
  <div className='lastPurchase--container'>
    <div className='view_profile--title'>
      <label htmlFor='lastPurchase--label'>
        <span>Seguros Contratados</span>
      </label>
    </div>
    <div className='backgroundLastPurchase'>
      <table className='tableLastPurchase'>
        <tr>
          <th>Código</th>
          <th>Descrição</th>
          <th>Valor Parcela</th>
          <th>Parcela</th>
          <th>Ações</th>
        </tr>
          <tr>
              <td>1</td>
              <td>Teste</td>
              <td>R$ 2.000,00</td>
              <td>3</td>
              <td>...</td>
          </tr>
          <tr>
              <td>1</td>
              <td>Teste</td>
              <td>R$ 2.000,00</td>
              <td>3</td>
              <td>...</td>
          </tr>
          <tr>
              <td>1</td>
              <td>Teste</td>
              <td>R$ 2.000,00</td>
              <td>3</td>
              <td>...</td>
          </tr>
          <tr>
              <td>1</td>
              <td>Teste</td>
              <td>R$ 2.000,00</td>
              <td>3</td>
              <td>...</td>
          </tr>
          <tr>
              <td>1</td>
              <td>Teste</td>
              <td>R$ 2.000,00</td>
              <td>3</td>
              <td>...</td>
          </tr>
          <tr>
              <td>1</td>
              <td>Teste</td>
              <td>R$ 2.000,00</td>
              <td>3</td>
              <td>...</td>
          </tr>
          <tr>
              <td>1</td>
              <td>Teste</td>
              <td>R$ 2.000,00</td>
              <td>3</td>
              <td>...</td>
          </tr>
          <tr>
              <td>1</td>
              <td>Teste</td>
              <td>R$ 2.000,00</td>
              <td>3</td>
              <td>...</td>
          </tr>
      </table>
    </div>
  </div>
  </> )
}

export default SegurosContratados
