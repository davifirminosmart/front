import React from 'react'
import PropTypes from 'prop-types'

function button ({ onClick, children }) {
  return (
    <span className='btn btn--primary' onClick={onClick}>
      {children}
    </span>
  )
}

button.propTypes = {
  onClick: PropTypes.func,
  children: PropTypes.string.isRequired
}
export default button
