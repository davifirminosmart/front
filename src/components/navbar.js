import React, { useState, useEffect } from 'react'
import LogoConductor from './logoConductor'
import LogoConductorMobile from './logoMobileConductor'
import { useSelector, useDispatch } from 'react-redux'
import { changeModal } from 'store/actions/actionsClient'
import { Link, Redirect } from 'react-router-dom'
import { fillDependentes } from 'store/actions/actionsDependente'
import { fillCardDependente } from 'store/actions/actionsCardDependente'
import { fillTicket } from 'store/actions/actionsTicket'

function Navbar (props) {
  const roleUser = useSelector(state => state.user.user.role)
  const name = useSelector(state => state.user.user.username)
  const dispatch = useDispatch()
  const [links, setLinks] = useState([])
  const [redirect, setRedirect] = useState(false)

  console.log('Role',roleUser)

  useEffect(() => {
      if (roleUser === 'admin') {
        setLinks(_l => [
          { name: 'Atender', active: true, handleClick: event => { linkAtender() }, icon: 'lni lni-consulting', to: '/principal' },
          { name: 'Seguros', active: false, icon: 'lni lni-gift', handleClick: event => goSeguros(), to: '/seguros' },
          { name: 'Usuários', active: false, icon: 'lni lni-users', handleClick: event => goUsers(), to: '/usuarios' }
        ])
      } else {
        setLinks(_l => [
          { name: 'Atender', active: true, handleClick: event => { linkAtender() }, icon: 'lni lni-consulting', to: '/principal' },
          { name: 'Seguros', active: false, icon: 'lni lni-gift', handleClick: event => goSeguros(), to: '/seguros' }
        ])
      }
    }, [roleUser])

  function linkAtender () {
    dispatch(changeModal(true))
    setActive(0)
  }

  async function goUsers () {
    await setActive(2)
  }

  async function goSeguros () {
    await setActive(1)
  }

  async function setActive (index) {
    await setLinks(links => {
      const newLink = links.map((_l, i) => {
        _l.active = false
        if (Number(i) === Number(index)) _l.active = true
        return _l
      })
      console.log('chamei',newLink)
      return newLink
    })
  }

  async function logout () {
    dispatch(fillTicket({
      ticket: {
        protocol: ''
      }
    }))
    dispatch(changeModal(true))
    dispatch(fillCardDependente([]))
    dispatch(fillDependentes([]))
    setRedirect(true)
  }
  return (
    <>
      {redirect && <Redirect to='/' />}
      <nav className='navbar'>
        <div className='navbar--logo'>
          <div className='navbar--logo--desktop'>
            <LogoConductor white height='120px' />
          </div>
          <div className='navbar--logo--mobile'>
            <LogoConductorMobile />
          </div>
        </div>
        <ul className='navbar--list'>
          {links.map((_l, index) =>
            _l.active ? <Link to={_l.to} className='active' key={index} onClick={_l.handleClick}> <i className={_l.icon} /> <span>{_l.name}</span>   </Link> : <Link to={_l.to} key={index} onClick={_l.handleClick}> <i className={_l.icon} /> <span>{_l.name} </span> </Link>
          )}
        </ul>
        <div className='navbar--logout'>
          <span>{name}</span>
          <span onClick={logout} className='navbar--logout--icon'>
            <i className='lni lni-power-switch' />
          </span>
        </div>
      </nav>
    </>
  )
}

export default Navbar
