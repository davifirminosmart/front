import React, { useEffect, useState } from 'react'
import { useSelector } from 'react-redux'
import Api from 'api'
import Moment from 'moment'

function TimeLine () {
  const [timeline, setTimeline] = useState([])
  const client = useSelector(state => state.clients.client)
  useEffect(() => {
    async function fetchData () {
      const tl = await Api.get('timeline', { id: client.id })
      setTimeline(_t => tl.timeline)
    } fetchData()
  }, [client])

  return (
    <>
      <div className='timeline-line'>
        <hr />
      </div>
      <div className='timeline--container'>
        <div className='timeline--titlecontainer'>
          <span>Timeline  <div><i /> <i /> <i /> </div> </span>
        </div>
        <ul className='timeline'>
          {timeline.map((item, index) =>
            <li className='timeline-item' key={index}>
              <div className='timeline-info'>
                <span>{Moment(item.created_at).format('DD/MM/YYYY HH:mm')}</span>
              </div>
              <div className='timeline-marker' />
              <div className='timeline-content'>
                <h3 className='timeline-title'>{item.title}</h3>
                <p>{item.description}</p>
                <p className='created'> {item.username}</p>
                <p className='protocol'>{item.protocol}</p>
              </div>
            </li>)}
        </ul>
      </div>
    </>
  )
}

export default TimeLine
