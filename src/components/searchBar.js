import React, { useState } from 'react'
import { NotificationManager } from 'react-notifications'
import { VerticalLoader } from 'components/loader'
import Button from 'components/button'
import { useSelector } from 'react-redux'

function SearchBar () {
  const [loading, setLoading] = useState()
  const protocol = useSelector(state => state.tickets.ticket.protocol)
  function setLoader () {
    setLoading(_l => true)
    setTimeout(() => {
      NotificationManager.error('Calma Fi!!!!')
      setLoading(_l => false)
    }, 2000)
  }
  return (
    <>
      <div className='searchBar'>
        <div className='searchBar--protocol'>
          {protocol &&
            <>
              <span>Protocolo: </span>
              <span>{protocol}</span>
            </>}
        </div>
      </div>
    </>
  )
}

export default SearchBar
