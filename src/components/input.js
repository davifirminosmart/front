/* eslint-disable react/jsx-closing-tag-location */
import React from 'react'
import PropTypes from 'prop-types'
import MaskedInput from 'react-maskedinput'

function Input ({ id, label, type, onChange, value, mask, disabled }) {
  return (
    mask
      ? <div className='input'>
        <div className='input--container'>
          <div className='input--container--input'>
            <MaskedInput type={type} id={id} placeholder=' ' onChange={onChange} value={value} mask={mask} disabled={disabled} />
            <label htmlFor={id}>{label}</label>
          </div>
        </div>
      </div>
      : <div className='input'>
        <div className='input--container'>
          <div className='input--container--input'>
            <input type={type} id={id} placeholder=' ' onChange={onChange} value={value} disabled={disabled} />
            <label htmlFor={id}>{label}</label>
          </div>
        </div>
      </div>
  )
}

Input.propTypes = {
  id: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  onChange: PropTypes.func,
  value: PropTypes.string,
  mask: PropTypes.string,
  disabled: PropTypes.bool
}

export default Input
