import React from 'react'
import { useSelector } from 'react-redux'

export default function DependentesTable () {
  const dependentes = useSelector(state => state.dependentes.dependentes)
  return (
    <>
      <div className='dependentes'>
        <span>Dependentes</span>
        <div className='dependentes--table'>
          {dependentes.length !== 0 &&
            <table>
              <tr>
                <th>Nome</th>
                <th>CPF</th>
                <th>Parentesco</th>
                <th>Data de Nascimento</th>
              </tr>
              <tbody>
                {dependentes.map((_d, index) => (
                  <tr key={index}>
                    <td> {_d.nome} </td>
                    <td> {_d.cpf} </td>
                    {Number(_d.grauParentesco) === 1 && <td>Pai</td>}
                    {Number(_d.grauParentesco) === 2 && <td>Mãe</td>}
                    {Number(_d.grauParentesco) === 3 && <td>Filho</td>}
                    {Number(_d.grauParentesco) === 4 && <td>Irmão</td>}
                    {Number(_d.grauParentesco) === 5 && <td>Conjuge</td>}
                    {Number(_d.grauParentesco) === 6 && <td>Outro</td>}
                    {Number(_d.grauParentesco) === -1 && <td>Outro</td>}
                    <td>{_d.nascimento}</td>
                  </tr>
                ))}
              </tbody>
            </table>}
          {dependentes.length === 0 && <div className='dependentes--vazio'> Nenhum dependente encontrado</div>}
        </div>
      </div>
    </>
  )
}
