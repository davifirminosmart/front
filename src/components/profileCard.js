import React, { useEffect, useState } from 'react'
import Can from 'layout/can'
import { useSelector, useDispatch } from 'react-redux'
import Api from 'api'
import { NotificationManager } from 'react-notifications'
import { changeModalProfile } from 'store/actions/actionsClient'

function ProfileCard () {
  const dispatch = useDispatch()
  const client = useSelector(state => state.clients.client)
  const [contacts, setContacts] = useState([])
  try {
    useEffect(() => {
      async function fetchData () {
        const { contacts } = await Api.get('contatos', { id: client.id })
        setContacts(_c => contacts)
      } fetchData()
    }, [client])
  } catch (error) {
    NotificationManager.error(error.message)
  }

  function openModal (open, edit) {
    dispatch(changeModalProfile({ open, edit }))
  }

  return (
    <>
      <Can permission='view_profile'>
        <div className='profile'>
          <div className='view_profile--title'>
            <label htmlFor='view_profile--label'>
              <span>Dados</span> <div className='container-icon'><i /> <i /> <i /></div>
            </label>
            <input type='checkbox' id='view_profile--label' />
            <div className='view_profile--menu'>
              <ul>
                <Can permission='edit_profile'>
                  <li onClick={() => { openModal(true, true) }}>Editar</li>
                </Can>
                <Can permission='view_profile'>
                  <li onClick={() => { openModal(true, false) }}>Ver detalhes</li>
                </Can>
              </ul>
            </div>
          </div>

          <div className='card'>
            <div className='avatar'>
              <div className='thumb'>
                {`${client.name.split(' ')[0][0]}${client.name.split(' ')[1][0]}`}
              </div>
              <div className='avatar--title'>
                {client.name}
              </div>
            </div>
            <div className='row'>
              <div className='card--body'>

                <div className='card--body--column'>
                  <div className='card--body--item'>
                    <span className='label'>Nome</span>
                    <span className='value'>{client.name}</span>
                  </div>
                  <div className='card--body--item'>
                    <span className='label'>cpf</span>
                    <span className='value'>{client.cpf}</span>
                  </div>
                  <div className='card--body--item'>
                    <span className='label'>email</span>
                    <span className='value'>{client.email}</span>
                  </div>
                  <div className='card--body--item'>
                    <span className='label'>rg</span>
                    <span className='value'>{client.rg}</span>
                  </div>
                </div>

                <div className='card--body--column'>
                  {client.address_residencial &&
                    <div className='card--body--item'>
                      <span className='label'>Endereço Residencial</span>
                      <span className='value' style={{ textTransform: 'lowercase' }}>
                        {`${client.address_residencial.logradouro}, ${client.address_residencial.numero} - ${client.address_residencial.bairro} ${client.address_residencial.cidade} - ${client.address_residencial.uf}`}
                      </span>
                    </div>}
                </div>

                <div className='card--body--column'>
                  {contacts.map((_c, index) => (
                    <div className='card--body--item' key={index}>
                      <span className='label' style={{ textTransform: 'capitalize' }}>
                        {String(_c.tipo).toLowerCase()}
                      </span>
                      <span className='value'>
                        {_c.numero}
                      </span>
                    </div>))}

                  {client.address_comercial &&
                    <div className='card--body--item'>
                      <span className='label'>Endereço Comercial</span>
                      <span className='value' style={{ textTransform: 'lowercase' }}>
                        {`${client.address_comercial.logradouro}, ${client.address_comercial.numero} - ${client.address_comercial.bairro} ${client.address_comercial.cidade} - ${client.address_comercial.uf}`}
                      </span>
                    </div>}

                  {client.address_outro &&
                    <div className='card--body--item'>
                      <span className='label'>Endereço Outro</span>
                      <span className='value' style={{ textTransform: 'lowercase' }}>
                        {`${client.address_outro.logradouro}, ${client.address_outro.numero} - ${client.address_outro.bairro} ${client.address_outro.cidade} - ${client.address_outro.uf}`}
                      </span>
                    </div>}
                </div>

              </div>
            </div>
            {/* <div className='avatar'>
              <div className='thumb'>
                <i className='lni lni-user' />
              </div>

              <div className='data'>
                <span className='data'>{client.name}</span>
                {/* <span className='data'>Sexo: Masculino</span> */}
            {/* </div>
            </div>  */}

            {/* <div className='card--body'>
              <div className='card--body--column'>
                <span className='field'> RG </span>
                <span className='field'> CPF </span>
                <span className='field'> Email </span>
                {contacts.map((_c, index) => <span className='field' style={{ textTransform: 'capitalize' }} key={index}> {String(_c.tipo).toLowerCase()} </span>)}
                {client.address_residencial && <span className='field'> Endereço Residencial </span>}
                {client.address_comercial && <span className='field'> Endereço Comercial </span>}
                {client.address_outro && <span className='field'> Endereço de Envio </span>}
              </div>

              <div className='card--body--column'>
                <span className='value'>{client.rg}</span>
                <span className='value'>{client.cpf}</span>
                <span className='value'>{client.email}</span>
                {contacts.map((_c, index) => <span className='value' key={index}> {String(_c.numero)} </span>)}
                {client.address_residencial &&
                  <span className='value' style={{ textTransform: 'lowercase' }}>
                    {`${client.address_residencial.logradouro}, ${client.address_residencial.numero} - ${client.address_residencial.bairro} ${client.address_residencial.cidade} - ${client.address_residencial.uf}`}
                  </span>}
                {client.address_comercial &&
                  <span className='value' style={{ textTransform: 'lowercase' }}>
                    {`${client.address_comercial.logradouro}, ${client.address_comercial.numero} - ${client.address_comercial.bairro} ${client.address_comercial.cidade} - ${client.address_comercial.uf}`}
                  </span>}
                {client.address_outro &&
                  <span className='value' style={{ textTransform: 'lowercase' }}>
                    {`${client.address_outro.logradouro}, ${client.address_outro.numero} - ${client.address_outro.bairro} ${client.address_outro.cidade} - ${client.address_outro.uf}`}
                  </span>}
              </div>
            </div> */}

          </div>
        </div>
      </Can>
    </>
  )
}
export default ProfileCard
