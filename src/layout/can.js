import React from 'react'
import { useSelector } from 'react-redux'
import PropTypes from 'prop-types'

export default function Can ({ permission, children }) {
  const _p = useSelector(state => state.user.user.permissions)
  return (
    <>
      {_p.includes(permission) ? children : null}
    </>
  )
}

Can.propTypes = {
  permission: PropTypes.string.isRequired,
  children: PropTypes.func
}
