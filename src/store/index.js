import { createStore, combineReducers } from 'redux'
import clientReducer from 'store/reducer/reducerClient'
import ticketReducer from 'store/reducer/reducerTicket'
import userReducer from 'store/reducer/reducerUser'
import cardReducer from 'store/reducer/reducerCard'
import cardsDependente from 'store/reducer/reducerCardDependente'
import dependentesReducer from 'store/reducer/reducerDependente'
import tipoBloqueioReducer from 'store/reducer/reducerTipoBloqueio'
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage/session'

const persist_config = {
  key: 'root',
  storage,
  blacklist: ['cardsDepentende']
}

const reducers = combineReducers({
  clients: clientReducer,
  tickets: ticketReducer,
  user: userReducer,
  cards: cardReducer,
  cardsDepentende: cardsDependente,
  dependentes: dependentesReducer,
  tipoBloqueio: tipoBloqueioReducer

})

const reducer = persistReducer(persist_config, reducers)

const store = createStore(
  reducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)

const persistor = persistStore(store)

export { store, persistor }
