export const FILL_TIPOS_BLOQUEIO = 'FILL_TIPOS_BLOQUEIO'

export function fillTiposBloqueio (tipos) {
  return {
    type: FILL_TIPOS_BLOQUEIO,
    tipos
  }
}
