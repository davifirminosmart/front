export const FILL_CARD = 'FILL_CARD'
export const ADD_CARD = 'ADD_CARD'
export const ALTER_CARD = 'ALTER_CARD'

export function fillCard (card) {
  return {
    type: FILL_CARD,
    card
  }
}

export function addCard (card) {
  return {
    type: ADD_CARD,
    card
  }
}


export function alterCard (card) {
  return {
    type: ALTER_CARD,
    card
  }
}
