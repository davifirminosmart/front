export const FILL_CARD_DEPENDENTE = 'FILL_CARD_DEPENDENTE'
export const ADD_CARD_DEPENDENTE = 'ADD_CARD_DEPENDENTE'
export const ALTER_CARD_DEPENDENTE = 'ALTER_CARD_DEPENDENTE'

export function fillCardDependente (card) {
  return {
    type: FILL_CARD_DEPENDENTE,
    card
  }
}

export function addCardDependente (card) {
  return {
    type: ADD_CARD_DEPENDENTE,
    card
  }
}

  export function alterCardDependente (card) {
    return {
      type: ALTER_CARD_DEPENDENTE,
      card
    }
  }
