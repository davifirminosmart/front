export const FILL_DEPENDENTES = 'FILL_DEPENDENTES'
export const ADD_DEPENDENTES = 'ADD_DEPENDENTES'

export function fillDependentes (dependente) {
  return (
    {
      type: FILL_DEPENDENTES,
      dependente
    }
  )
}


export function addDependentes (dependente) {
  return (
    {
      type: ADD_DEPENDENTES,
      dependente
    }
  )
}
