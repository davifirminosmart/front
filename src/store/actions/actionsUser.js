export const FILL_USER = 'FILL_USER'

export function fillUser (user) {
  return ({
    type: FILL_USER,
    user
  })
}
