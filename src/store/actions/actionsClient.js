export const CHANGE_MODAL = 'CHANGE_MODAL'
export const FILL_CLIENT = 'FILL_CLIENT'
export const CHANGE_MODAL_PROFILE = 'CHANGE_MODAL_PROFILE'

export function changeModal (status) {
  return ({
    type: CHANGE_MODAL,
    status
  })
}

export function changeModalProfile (status) {
  return ({
    type: CHANGE_MODAL_PROFILE,
    status
  })
}

export function fillClient (client) {
  return ({
    type: FILL_CLIENT,
    client
  })
}
