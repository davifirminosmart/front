export const FILL_TICKET = 'FILL_TICKET'

export function fillTicket (ticket) {
  return ({
    type: FILL_TICKET,
    ticket
  })
}
