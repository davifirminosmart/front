import { FILL_CARD_DEPENDENTE, ADD_CARD_DEPENDENTE, ALTER_CARD_DEPENDENTE } from 'store/actions/actionsCardDependente'

const INITIAL_STATE = {
  cards: []
}

function cardsDependente (state = INITIAL_STATE, action) {
  switch (action.type) {
    case FILL_CARD_DEPENDENTE:
      return {
        ...state,
        cards: action.card
      }
    case ADD_CARD_DEPENDENTE:
      return {
        ...state,
        cards: [...state.cards, action.card]
      }
    case ALTER_CARD_DEPENDENTE:
      return {
        ...state,
        cards: state.cards.map(_c => {
          if(_c.id === action.card.id) return action.card
          return _c
        })
      }
    default:
      return state
  }
}

export default cardsDependente
