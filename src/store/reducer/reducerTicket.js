import { FILL_TICKET } from 'store/actions/actionsTicket'

const INITIAL_STATE = {
  ticket: {
    protocol: ''
  }
}
function tickets (state = INITIAL_STATE, action) {
  switch (action.type) {
    case FILL_TICKET:
      return {
        ...state,
        ticket: action.ticket
      }
    default:
      return state
  }
}

export default tickets
