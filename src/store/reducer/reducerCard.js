import { FILL_CARD, ADD_CARD, ALTER_CARD } from 'store/actions/actionsCard'

const INITIAL_STATE = {
  cards: []
}

function cards (state = INITIAL_STATE, action) {
  switch (action.type) {
    case FILL_CARD:
      return {
        ...state,
        cards: [action.card]
      }
    case ADD_CARD:
      return {
        ...state,
        cards: [...state.cards, action.card]
      }
    case ALTER_CARD:
      return {
        ...state,
        cards: state.cards.map(_c => {
          if(_c.id === action.card.id) return action.card
          return _c
        })
      }
    default:
      return state
  }
}

export default cards
