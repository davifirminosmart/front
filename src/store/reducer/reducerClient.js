import { CHANGE_MODAL, FILL_CLIENT, CHANGE_MODAL_PROFILE } from 'store/actions/actionsClient'

const INITIAL_STATE = {
  modal: {
    open: true,
    profileModal: {
      open: false,
      edit: false
    }
  },
  client: []
}

function clients (state = INITIAL_STATE, action) {
  switch (action.type) {
    case CHANGE_MODAL:
      return {
        ...state,
        modal: {
          ...state.modal,
          open: action.status
        }
      }
    case FILL_CLIENT:
      return {
        ...state,
        client: action.client
      }
    case CHANGE_MODAL_PROFILE:
      return {
        ...state,
        modal: {
          ...state.modal,
          profileModal: action.status
        }
      }
    default:
      return state
  }
}

export default clients
