import { FILL_DEPENDENTES, ADD_DEPENDENTES } from 'store/actions/actionsDependente'

const INITIAL_STATE = {
  dependentes: []
}

function cards (state = INITIAL_STATE, action) {
  switch (action.type) {
    case FILL_DEPENDENTES:
      return {
        ...state,
        dependentes: action.dependente
      }
    case ADD_DEPENDENTES:
      return {
        ...state,
        dependentes: [...state.dependentes, action.dependente]
      }
    default:
      return state
  }
}

export default cards
