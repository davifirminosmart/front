import { FILL_USER } from 'store/actions/actionsUser'

const INITIAL_STATE = {
  user: {}
}

export default function user (state = INITIAL_STATE, action) {
  switch (action.type) {
    case FILL_USER:
      return {
        ...state,
        user: action.user
      }
    default:
      return state
  }
}
