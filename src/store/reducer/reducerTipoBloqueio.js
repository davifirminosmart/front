import { FILL_TIPOS_BLOQUEIO } from 'store/actions/actionsTipoBloqueio'

const INITIAL_STATE = {
  tipos: []
}

function tiposBloqueio (state = INITIAL_STATE, action) {
  switch (action.type) {
    case FILL_TIPOS_BLOQUEIO:
      return {
        ...state,
        tipos: action.tipos
      }
    default:
      return state
  }
}

export default tiposBloqueio
