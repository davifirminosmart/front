const heroku = 'https://mighty-forest-18459.herokuapp.com'
const local = 'http://localhost:3333'
const prod = 'http://52.44.231.16:3333'

const baseUrl = local
class Api {
  constructor () {
    this.jwt = ''
    this.jwtAdmin = ''
  }

  setJwt (jwt) {
    this.jwt = jwt
    sessionStorage.setItem('jwt', jwt)
  }

  setJwtAdmin (jwt) {
    this.jwtAdmin = jwt
    sessionStorage.setItem('jwtAdmin', jwt)
  }

  async get (url, params) {
    let _p = ''
    if (params) _p = this.serialize(params)
    const headers = this.headers({})
    return fetch(`${baseUrl}/${url}?${_p}`, {
      headers
    }).then(async res => {
      if (res.status.toString()[0] === '4' || res.status.toString()[0] === '5') {
        const message = await res.json()
        throw new Error(message.message)
      }
      return res.json()
    })
  }

  async post (url, params) {
    let body = {}
    if (params) body = params
    const headers = this.headers({})
    return fetch(`${baseUrl}/${url}`, {
      method: 'POST',
      headers,
      body: JSON.stringify(body)
    }).then(async res => {
      if (res.status.toString()[0] === '4' || res.status.toString()[0] === '5') {
        if (res.status.toString() === '403') throw new Error('Operação proibida para esse usuário')
        const message = await res.json()
        throw new Error(message.message)
      }
      return res.json()
    })
  }

  async put (url, params) {
    let body = {}
    if (params) body = params
    const headers = this.headers({})
    return fetch(`${baseUrl}/${url}`, {
      method: 'PUT',
      headers,
      body: JSON.stringify(body)
    }).then(async res => {
      if (res.status.toString()[0] === '4' || res.status.toString()[0] === '5') {
        if (res.status.toString() === '403') throw new Error('Operação proibida para esse usuário')
        const message = await res.json()
        throw new Error(message.message)
      }
      return res.json()
    })
  }

  serialize (obj) {
    var str = []
    for (var p in obj) {
      // eslint-disable-next-line no-prototype-builtins
      if (obj.hasOwnProperty(p)) {
        str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]))
      }
    }
    return str.join('&')
  }

  headers (headers) {
    const jwt = sessionStorage.getItem('jwt')
    const jwtAdmin = sessionStorage.getItem('jwtAdmin')
    headers = {
      'Content-Type': 'application/json',
      Authorization: `bearer ${jwt || jwtAdmin}`,
      ...headers
    }
    return headers
  }
}

export default new Api()
