import React from 'react'
import { Routes, AdminRoutes } from 'pages/routes'
import { Provider } from 'react-redux'
import { store } from 'store'
import 'react-responsive-modal/styles.css'

function App () {
  return (
    <>
      <Provider store={store}>
        <Routes />
        <AdminRoutes />
      </Provider>
    </>

  )
}

export default App
